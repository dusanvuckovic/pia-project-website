package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Festival;
import model.Media;
import model.additional.MediaH;
import model.additional.MediaH.MediaEnum;
import org.hibernate.Session;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import utility.MediaCacher;

@ManagedBean
@ViewScoped
public class AddFestivalMediaBean implements Serializable {

    public AddFestivalMediaBean() {
    }

    private final String STEP_ONE_FILE = "addnewfestivalfile",
            STEP_ONE_DATA = "addnewfestivaldata",
            STEP_TWO = "addnewfestivalmedia",
            STEP_THREE = "addnewfestivaloverview";

    private Festival festival;
    private List<MediaH> media;

    private void getFestivalAndMedia() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        festival = (Festival) s.createQuery("from Festival where currentStep <> 0").getResultList().get(0);
        List<Media> mediaList = s.createQuery("from Media where festivalID = :festivalID").setParameter("festivalID", festival.getFestivalID()).getResultList();
        s.getTransaction().commit();
        s.close();

        for (Media m : mediaList) {
            String saveLocation = MediaCacher.saveToLocation(m);
            MediaH mh = new MediaH(m, saveLocation, MediaEnum.DB_AS_IS);
            media.add(mh);
        }
    }

    @PostConstruct()
    public void initialize() {
        media = new ArrayList();
        getFestivalAndMedia();
    }

    public void handleFileUpload(FileUploadEvent event) {

        UploadedFile file = event.getFile();

        Media newMedia = new Media(festival.getFestivalID(), 0, file.getFileName(), file.getContentType(), file.getContents(), true);

        MediaH uploadedMedia = null;
        for (MediaH mediaElement : media) {
            if (mediaElement.getM().isSameImage(newMedia)) {
                uploadedMedia = mediaElement;
                break;
            }
        }
        
        if (uploadedMedia == null) {
            String saveLocation = MediaCacher.saveToLocation(newMedia);
            uploadedMedia = new MediaH(newMedia, saveLocation, MediaEnum.NEW_ADD_TO);
            media.add(uploadedMedia);
        } 
        
        else {
            if (uploadedMedia.getState() == MediaEnum.DB_DELETE) {
                uploadedMedia.setState(MediaEnum.DB_AS_IS);
            } 
            else if (uploadedMedia.getState() == MediaEnum.NEW_CANCELLED) {
                uploadedMedia.setState(MediaEnum.NEW_ADD_TO);
            }
        }

    }

    public boolean renderImages() {
        for (MediaH mediaH : media) {
            if (mediaH.getState() == MediaEnum.DB_AS_IS) {
                return true;
            }
            if (mediaH.getState() == MediaEnum.NEW_ADD_TO) {
                return true;
            }
        }
        return false;
    }
    
    private void saveMedia() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        for (MediaH med : media) {
            switch (med.getState()) {
                case DB_DELETE:
                    s.delete(med.getM());
                    break;
                case NEW_ADD_TO:
                    s.save(med.getM());
                    break;
                default:
                    break;
            }
        }    
        s.getTransaction().commit();
        s.close();
    }
    
    public String goToStep(int step) {
        saveMedia();
        if (step == 1)
            return "addnewfestivaldata";
        else if (step == 3)
            return "addnewfestivaloverview";    
        return "";
    }

    public String process() {
        saveMedia();
        
        festival.setCurrentStep(2);
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        s.saveOrUpdate(festival);
        
        s.getTransaction().commit();
        s.close();
        
        return STEP_THREE;
    }
    
    public List<MediaH> getImagesToRender() {
        List<MediaH> list = new ArrayList<>();
        for (MediaH mh: media) {
            if (mh.shouldRender())
                list.add(mh);
        }
        return list;
    }

    public void clearImagesFromGallery() {
        for (MediaH med : media) {
            switch (med.getState()) {
                case NEW_ADD_TO:
                    med.setState(MediaEnum.NEW_CANCELLED);
                    break;
                case DB_AS_IS:
                    med.setState(MediaEnum.DB_DELETE);
                    break;
                default:
                    break;
            }
        }
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public List<MediaH> getMedia() {
        return media;
    }

    public void setMedia(List<MediaH> media) {
        this.media = media;
    }

}
