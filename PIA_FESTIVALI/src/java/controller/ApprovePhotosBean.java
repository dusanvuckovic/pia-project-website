package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Media;
import model.additional.MediaH;
import org.hibernate.Session;
import utility.MediaCacher;

@ManagedBean
@ViewScoped
public class ApprovePhotosBean implements Serializable {

    private List<MediaH> media;
    
    public ApprovePhotosBean() {
        
        media = new ArrayList();
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        List<Media> mediaS = s.createQuery("from Media where approved = 0").getResultList();
        for (Media med: mediaS) {
            String location = MediaCacher.saveToLocation(med);
            media.add(new MediaH(med, location));
        }
        
        s.getTransaction().commit();
        s.close();
    }
    
    public String approve() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        for (MediaH m: media) {
            if (m.getM().isApproved())
                s.update(m.getM());
        }
        
        s.getTransaction().commit();
        s.close();
        return "admin";
    }

    public List<MediaH> getMedia() {
        return media;
    }

    public void setMedia(List<MediaH> media) {
        this.media = media;
    }
    
    
    
}
