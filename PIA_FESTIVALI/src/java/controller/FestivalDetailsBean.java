package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Festival;
import model.Media;
import model.Performance;
import model.Rating;
import model.additional.MediaH;
import org.hibernate.Session;
import utility.MediaCacher;

@ManagedBean
@RequestScoped
public class FestivalDetailsBean implements Serializable {

    private Festival festival;
    private List<Performance> performance;
    private List<MediaH> media;
    private List<Rating> ratings;
    
    public FestivalDetailsBean() {
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        festival = (Festival) s.createQuery("from Festival where renderSelect = 1").uniqueResult();
        festival.setRenderSelect(false);
        festival.incrementViewCount();
        
        
        performance = s.createQuery("from Performance where festivalID = :festivalID")
                .setParameter("festivalID", festival.getFestivalID())
                .getResultList();
        
        List<Media> mediaL = s.createQuery("from Media where festivalID = :festivalID")
                .setParameter("festivalID", festival.getFestivalID())
                .getResultList();        
        
        ratings = s.createQuery("from Rating where festivalID = :festivalID")
                .setParameter("festivalID", festival.getFestivalID())
                .getResultList(); 
        
        media = new ArrayList();
        for (Media m: mediaL)
            media.add(new MediaH(m, MediaCacher.saveToLocation(m)));
        
        s.update(festival);
        
        s.getTransaction().commit();
        s.close();        
        
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public List<Performance> getPerformance() {
        return performance;
    }

    public void setPerformance(List<Performance> performance) {
        this.performance = performance;
    }

    public List<MediaH> getMedia() {
        return media;
    }

    public void setMedia(List<MediaH> media) {
        this.media = media;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }
    
}
