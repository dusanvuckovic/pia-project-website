package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Festival;
import model.Reservation;
import model.additional.ReservationH;
import org.hibernate.Session;

@ManagedBean
@ViewScoped
public class CancelReservationsBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loggedIn;

    private List<String> options;
    
    private final String AS_IS = "Ignoriši",
            DENY = "Odbij";

    private List<ReservationH> reservations;

    public CancelReservationsBean() {
        options = new ArrayList();
        options.add(AS_IS);
        options.add(DENY);
    }

    @PostConstruct
    public void init() {
        

        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        reservations = new ArrayList();
        List<Reservation> reservs = s.createQuery("from Reservation where userID = :userID and denied = 0 and approved = 0")
                .setParameter("userID", loggedIn.getLoggedIn().getUserID()).getResultList();
        for (Reservation re : reservs) {
            Festival f = (Festival) s.createQuery("from Festival where festivalID = :festivalID")
                    .setParameter("festivalID", re.getFestivalID())
                    .uniqueResult();
            reservations.add(new ReservationH(re, loggedIn.getLoggedIn().getUsername(), f.getFestivalTitle(), Integer.toString(f.getFestivalYear())));
        }
        s.getTransaction().commit();
        s.close();

    }

    public String cancelReservations() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        for (ReservationH r : reservations) {
            if (r.getOutcome().equals(DENY)) {
                r.getR().setDenied(true);
                s.update(r.getR());
            }
        }
        s.getTransaction().commit();
        s.close();
        return "user";
    }

    public LoginBean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(LoginBean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public List<ReservationH> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationH> reservations) {
        this.reservations = reservations;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }
    
    

}
