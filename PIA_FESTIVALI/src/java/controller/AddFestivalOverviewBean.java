package controller;

import hibernate.HibernateUtility;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Festival;
import model.Media;
import model.Performance;
import org.hibernate.Session;
import utility.MediaCacher;
import utility.NotificationList;

@ManagedBean
@RequestScoped
public class AddFestivalOverviewBean {

    public AddFestivalOverviewBean() {
    }        
    
    private Festival festival;
    private List<String> media;
    private List<Performance> performances;
    
    @PostConstruct
    public void initialize() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        festival = (Festival) s.createQuery("from Festival where currentStep <> 0").getResultList().get(0);
        List<Media> m = s.createQuery("from Media where festivalID = :festivalID").setParameter("festivalID", festival.getFestivalID()).getResultList();
        performances = s.createQuery("from Performance where festivalID = :festivalID")
                .setParameter("festivalID", festival.getFestivalID())
                .getResultList();  
        Collections.sort(performances);
        
        s.getTransaction().commit();
        s.close();   
        
        media = new ArrayList();
        for (Media med: m) {
            media.add(MediaCacher.saveToLocation(med));
        }
    }
    
    public String process() {
        NotificationList nl = new NotificationList();
        festival.setCurrentStep(0);
        HibernateUtility.updateObjectInDB(festival);                
        
        nl.addInfoMessage("Uspešno dodat festival");
        return "admin";                
    }
    
    public String goToStep(int step) {
        if (step == 2)
            return "addnewfestivalmedia";
        else if (step == 1)
            return "addnewfestivaldata";
        return "";            
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public List<String> getMedia() {
        return media;
    }

    public void setMedia(List<String> media) {
        this.media = media;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }
    
    
    
    
}
