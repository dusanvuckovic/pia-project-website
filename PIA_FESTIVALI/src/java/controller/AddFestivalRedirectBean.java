package controller;

import hibernate.HibernateUtility;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Festival;
import org.hibernate.Session;


@RequestScoped
@ManagedBean
public class AddFestivalRedirectBean {
    
    public AddFestivalRedirectBean() {
    }
    
    private final String STEP_ONE_FILE = "addnewfestivalfile",
                         STEP_ONE_DATA = "addnewfestivaldata",
                         STEP_TWO      = "addnewfestivalmedia",
                         STEP_THREE    = "addnewfestivaloverview";
        
    public String redirect(boolean fileConstruct) {
        
        List<Festival> relevantFestivals;
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        relevantFestivals = s.createQuery("from Festival where currentStep <> 0").list();
        
        s.getTransaction().commit();
        s.close();
        
        if (relevantFestivals.isEmpty()) 
            if (fileConstruct)
                return STEP_ONE_FILE;
            else
                return STEP_ONE_DATA;
        
        if (relevantFestivals.size() != 1)
            return "";
        
        Festival f = relevantFestivals.get(0);
        switch (f.getCurrentStep()) {
            case 1: return STEP_TWO;
            case 2: return STEP_THREE;
            default: return "";
        }        
    
    }
    
}
