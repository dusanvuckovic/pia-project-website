package controller;

import csv.CSVParser;
import hibernate.HibernateUtility;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import json.JSONParser;
import model.Festival;
import model.FestivalTicketsPerDay;
import model.Performance;
import model.additional.PerformanceH;
import org.hibernate.Session;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import utility.NotificationList;

@ManagedBean
@ViewScoped
public class AddFestivalFileBean {

    private final String CSV_TYPE = ".csv",
            JSON_TYPE = ".json";

    @PostConstruct
    public void initialize() {
        render = false;
    }

    private Festival f;
    private List<PerformanceH> performances;

    private boolean render;

    public AddFestivalFileBean() {
    }

    public void handleFileUpload(FileUploadEvent event) {

        InputStream stream = null;
        try {
            UploadedFile file = event.getFile();
            String type = file.getFileName();
            type = type.substring(type.lastIndexOf("."), type.length());
            stream = file.getInputstream();
            switch (type) {
                case CSV_TYPE:
                    CSVParser csv = new CSVParser(stream);
                    f = csv.getFestival();
                    performances = csv.getPerformances();
                    render = true;
                    break;
                case JSON_TYPE:
                    JSONParser json = new JSONParser(stream);
                    f = json.getFestival();
                    performances = json.getPerformances();
                    render = true;
                    break;
                default:
                    break; //add error
            }
        } catch (IOException ex) {
            Logger.getLogger(AddFestivalFileBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(AddFestivalFileBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void saveToDatabase() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        f.setCurrentStep(1);
        Integer ID = (Integer) s.save(f);
        for (int i = 0; i < f.getNumberOfDays(); i++) {
            FestivalTicketsPerDay ft = new FestivalTicketsPerDay(ID, i + 1, f.getTicketNumberDay());
            s.save(ft);
        }
        for (PerformanceH p : performances) {
            Performance pDB = new Performance(f, p);
            s.save(pDB);
        }
        s.getTransaction().commit();
        s.close();
    }        

    public String addFiles() {
        NotificationList nl = new NotificationList();
        
        saveToDatabase();
        
        return "addnewfestivalmedia";
    }

    public boolean isRender() {
        return render;
    }

    public void setRender(boolean render) {
        this.render = render;
    }

}
