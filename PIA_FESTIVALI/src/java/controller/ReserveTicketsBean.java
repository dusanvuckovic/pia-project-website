package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.Festival;
import model.Reservation;
import org.hibernate.Session;
import utility.NotificationList;

@ManagedBean
@ViewScoped
public class ReserveTicketsBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loggedIn;

    private Integer selectedFestivalID;
    private List<Integer> festivalDays;

    private Festival f;

    private Integer selectedDay;
    private boolean entireFestival;
    private Integer numberOfReservations;

    private final String BAD_RESERVATION_NUMBER = "Morate zatražiti nenulti broj rezervacija!",
            BANNED_FROM_MAKING_RESERVATIONS = "Administrator vam je zabranio da pravite rezervacije!",
            TOO_MANY_RESERVATIONS = "Već ste postigli maksimum rezervacija!",
            TOO_MANY_MADE_RESERVATIONS = "Morate uzeti manji broj rezervacija!",
            DENIED_BEFORE = "Vaša rezervacija za ovaj festival je već odbijena!";

    @PostConstruct
    public void init() {
        Session s = hibernate.HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        f = (Festival) s.createQuery("from Festival where renderReservation = 1").uniqueResult();
        onFestivalSelectionChange();
        f.setRenderReservation(false);
        s.update(f);

        s.getTransaction().commit();
        s.close();

        numberOfReservations = 0;
        entireFestival = false;
    }

    public ReserveTicketsBean() {

    }

    public void onFestivalSelectionChange() {
        
        festivalDays = new ArrayList();
        for (int i = 1; i <= f.getNumberOfDays(); i++) {
            festivalDays.add(i);
        }
        selectedDay = festivalDays.get(0);
    }

    public Reservation getReservationOfUser() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        int userID = loggedIn.getLoggedIn().getUserID();
        int festivalID = f.getFestivalID();

        if (entireFestival) {
            selectedDay = 0;
        }

        Reservation r = (Reservation) s.createQuery("from Reservation where "
                + "userID = :userID and festivalID = :festivalID and reservationDay = :reservationDay")
                .setParameter("userID", userID)
                .setParameter("festivalID", festivalID)
                .setParameter("reservationDay", selectedDay)
                .uniqueResult();

        s.getTransaction().commit();
        s.close();

        if (r == null) {
            r = new Reservation(userID, festivalID, selectedDay, 0);
        }
        return r;
    }

    public String makeReservations() {
        NotificationList nl = new NotificationList();
        if (numberOfReservations <= 0) {
            nl.addErrorMessage(BAD_RESERVATION_NUMBER);
        }
        if (numberOfReservations > f.getMaxReservations()) {
            nl.addErrorMessage(TOO_MANY_RESERVATIONS);
        }
        if (loggedIn.getLoggedIn().bannedFromMakingReservations()) {
            nl.addErrorMessage(BANNED_FROM_MAKING_RESERVATIONS);
        }

        if (nl.printOutMessagesIfError()) {
            return "";
        }

        Reservation r = getReservationOfUser();

        if (r.isDenied()) {
            nl.addErrorMessage(DENIED_BEFORE);
        }

        if (r.getNumberOfTickets() + numberOfReservations > f.getMaxReservations()) {
            nl.addErrorMessage(TOO_MANY_MADE_RESERVATIONS);
        }

        if (nl.printOutMessagesIfError()) {
            return "";
        }

        r.setNumberOfTickets(r.getNumberOfTickets() + numberOfReservations);
        HibernateUtility.saveOrUpdateObjectInDB(r);

        if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";
        } else {
            nl.printOutMessages();
            return "user";
        }

    }

    public LoginBean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(LoginBean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Integer getSelectedFestivalID() {
        return selectedFestivalID;
    }

    public void setSelectedFestivalID(Integer selectedFestivalID) {
        this.selectedFestivalID = selectedFestivalID;
    }

    public List<Integer> getFestivalDays() {
        return festivalDays;
    }

    public void setFestivalDays(List<Integer> festivalDays) {
        this.festivalDays = festivalDays;
    }

    public Integer getSelectedDay() {
        return selectedDay;
    }

    public void setSelectedDay(Integer selectedDay) {
        this.selectedDay = selectedDay;
    }

    public boolean isEntireFestival() {
        return entireFestival;
    }

    public void setEntireFestival(boolean entireFestival) {
        this.entireFestival = entireFestival;
    }

    public Integer getNumberOfReservations() {
        return numberOfReservations;
    }

    public void setNumberOfReservations(Integer numberOfReservations) {
        this.numberOfReservations = numberOfReservations;
    }

    public Festival getF() {
        return f;
    }

    public void setF(Festival f) {
        this.f = f;
    }        

}
