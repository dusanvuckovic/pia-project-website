package controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import model.Performance;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import utility.Conversions;
import utility.NotificationList;

@ManagedBean
@SessionScoped
public class AddNewFestivalBean implements Serializable {

    private final String WEBPAGE = "addnewfestival";

    private Date now;
    private PerformanceH performance;
    private List<PerformanceH> performances;
        
    private int currentStep;

    private StreamedContent sc;
    private List<FileH> uploadedFiles;

    @PostConstruct
    public void initialize() {
        currentStep = 1;
        now = Date.from(Instant.now());
        performance = new PerformanceH();        
        performances = new ArrayList();
        performances.add(new PerformanceH("test", Date.from(Instant.now()), Date.from(Instant.now())));
        performances.add(new PerformanceH("test2", Date.from(Instant.now()), Date.from(Instant.now())));
        uploadedFiles = new ArrayList();
    }

    public class PerformanceH implements Serializable {

        private String performer;
        private Date from;
        private Date to;

        public PerformanceH() {
        }

        public PerformanceH(String performer, Date from, Date to) {
            this.performer = performer;
            this.from = from;
            this.to = to;
        }

        public String getPerformer() {
            return performer;
        }

        public void setPerformer(String performer) {
            this.performer = performer;
        }

        public Date getFrom() {
            return from;
        }

        public void setFrom(Date from) {
            this.from = from;
        }

        public Date getTo() {
            return to;
        }

        public void setTo(Date to) {
            this.to = to;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 43 * hash + Objects.hashCode(this.performer);
            hash = 43 * hash + Objects.hashCode(this.from);
            hash = 43 * hash + Objects.hashCode(this.to);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final PerformanceH other = (PerformanceH) obj;
            if (!Objects.equals(this.performer, other.performer)) {
                return false;
            }
            if (!Objects.equals(this.from, other.from)) {
                return false;
            }
            if (!Objects.equals(this.to, other.to)) {
                return false;
            }
            return true;
        }
    }
    
    public class FileH implements Serializable {
        private byte[] file;
        private String filename;
        private String contentType;

        public FileH() {
        }

        public FileH(byte[] file, String filename, String contentType) {
            this.file = file;
            this.filename = filename;
            this.contentType = contentType;
        }

        public byte[] getFile() {
            return file;
        }

        public void setFile(byte[] file) {
            this.file = file;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 47 * hash + Arrays.hashCode(this.file);
            hash = 47 * hash + Objects.hashCode(this.filename);
            hash = 47 * hash + Objects.hashCode(this.contentType);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final FileH other = (FileH) obj;
            if (!Objects.equals(this.filename, other.filename)) {
                return false;
            }
            if (!Objects.equals(this.contentType, other.contentType)) {
                return false;
            }
            if (!Arrays.equals(this.file, other.file)) {
                return false;
            }
            return true;
        }                

    }

    public AddNewFestivalBean() {
    }

    public String printOutDate(LocalDateTime ldt) {
        return ldt.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));

    }

    /*
    public void createNew() {
        NotificationList nl = new NotificationList();
        
        FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "zdravo", ""));
        LocalDateTime from_dt, to_dt;
        
        if (performance.from.after(performance.to)) {
            nl.addErrorMessage("Početak svirke ne može biti pre kraja!");
        }

        from_dt = Conversions.toLDT(performance.from);
        to_dt = Conversions.toLDT(performance.to);
        long minutes = ChronoUnit.MINUTES.between(to_dt, from_dt);
        System.out.println(minutes);
        nl.addErrorMessage("prva razlika je" + Long.toString(minutes));
        minutes = ChronoUnit.MINUTES.between(from_dt, to_dt);
        nl.addErrorMessage("druga razlika je" + Long.toString(minutes));
        if (minutes > 180) {
            nl.addErrorMessage("Koncert ne može trajati duže od 3 sata!");
        }

        if (nl.errorsExist()) {
            nl.printOutMessages();
            return;
        }
          
        if (!performances.contains(performance)) {
            performances.add(performance);
            performance = new PerformanceH();
            nl.addInfoMessage("Uspešno dodat performans!");
        } else {
            nl.addErrorMessage("Već postoji ovakav performans!");
        }
        nl.printOutMessages();
    }
    
    public String reinit() {
        performance = new PerformanceH();
        return null;
    }
    
     */

    public void createNew() {
        if (performance.from == null || performance.to == null || performance.performer == null || "".equals(performance.performer)) {
            FacesMessage msg = new FacesMessage("Dublicated", "Nepotpuno!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        if (performance.from.before(performance.to)) {
            FacesMessage msg = new FacesMessage("Dublicated", "Nepotpuno!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;            
        }
        if (performances.contains(performance)) {
            FacesMessage msg = new FacesMessage("Dublicated", "This book has already been added");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            performances.add(performance);
            performance = new PerformanceH();
        }
    }

    public String reinit() {
        performance = new PerformanceH();

        return null;
    }

    public String lastStep() {
        switch (currentStep) {
            case 2:
                currentStep = 1;
                break;
            case 3:
                currentStep = 2;
                break;
            case 4:
                currentStep = 3;
                break;
            default:
                break;
        }
        return WEBPAGE + Integer.toString(currentStep);
    }

    public String nextStep() {
        NotificationList nl = new NotificationList();
        switch (currentStep) {
            case 1:
                if (performances.isEmpty()) {
                    nl.addErrorMessage("Morate imati makar jedan performans!");
                } else {
                    currentStep = 2;
                }
                break;
            case 2:
                currentStep = 3;
                break;
            default:
                break;
        }
        nl.printOutMessages();
        return WEBPAGE + Integer.toString(currentStep);
    }

    public void handleFileUpload(FileUploadEvent event) {

        UploadedFile file = event.getFile();

        try {            
            sc = new DefaultStreamedContent(file.getInputstream(), file.getContentType(), file.getFileName());
            uploadedFiles.add(new FileH(file.getContents(), file.getFileName(), file.getContentType()));

            /*if (uploadedFiles.containsKey(filename))
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Filename " + filename + " already exists.", ""));
            if (uploadedFiles.containsValue(file))
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "This file is already uploaded!", ""));
            
            file.
            
            uploadedFiles.put(event.getFile().getFileName(), event.getFile());
            
            sc.getStream(
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully uploaded " + filename + "!", ""));
             */
        } catch (IOException ex) {
            Logger.getLogger(AddNewFestivalBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<StreamedContent> getImages() {
        List<StreamedContent> l = new ArrayList();
        for (FileH fh: uploadedFiles)
            l.add(new DefaultStreamedContent(new ByteArrayInputStream(fh.getFile()), fh.getContentType(), fh.getFilename()));                    
        return l;                
    }

    public void save() {

    }

    public PerformanceH getPerformance() {
        return performance;
    }

    public void setPerformance(PerformanceH performance) {
        this.performance = performance;
    }

    public List<PerformanceH> getPerformances() {
        return performances;
    }

    public void setPerformances(List<PerformanceH> performances) {
        this.performances = performances;
    }

    public Date getNow() {
        return now;
    }

    public void setNow(Date now) {
        this.now = now;
    }

    public List<FileH> getUploadedFiles() {
        return uploadedFiles;
    }

    public void setUploadedFiles(List<FileH> uploadedFiles) {
        this.uploadedFiles = uploadedFiles;
    }

    

    public int getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
    }

    public StreamedContent getSc() {
        return sc;
    }

    public void setSc(StreamedContent sc) {
        this.sc = sc;
    }
    
    

}
