package controller;


import hibernate.HibernateUtility;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import model.Message;
import model.Reservation;
import model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import utility.Conversions;
import utility.NotificationList;

@ManagedBean(name="loginBean")
@SessionScoped
public class LoginBean implements Serializable {
    
    private final String NO_USERNAME  = "Morate uneti korisničko ime!",
                         NO_PASSWORD  = "Morate uneti lozinku!",
                         BAD_USERNAME = "Pogrešno korisničko ime!",
                         BAD_PASSWORD = "Pogrešna lozinka!",
                         NOT_APPROVED = "Niste dobili odobrenje administratora!",
                         DENIED       =  "Administrator vam je zabranio pristup!";                         
    
    private String username = "";
    private String password = "";
    
    private User loggedIn = null;
    
    public static String processDate(LocalDateTime date) {
        return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }
    
    public static String processDate(Date date) {
        return processDate(Conversions.toLDT(date));
    }
        
    public LoginBean() {}        
    
    public String login() {     
        NotificationList nl = new NotificationList();
        
        if ("".equals(username))
            nl.addErrorMessage(NO_USERNAME);
        if ("".equals(password))
            nl.addErrorMessage(NO_PASSWORD);        
        if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";
        }
                                    
        Session s = HibernateUtility.getSessionFactory().openSession();         
        s.beginTransaction();
        List<User> users = s.createQuery("from User where username = :username", User.class).setParameter("username", username).getResultList();                        
        
        if (users.isEmpty())
            nl.addErrorMessage(BAD_USERNAME);
        else {
            User u = users.get(0);
            if (!u.getPassword().equals(password))
                nl.addErrorMessage(BAD_PASSWORD);
            else {
                if (u.isDenied()) {
                    nl.addErrorMessage(DENIED);
                }
                else if (!u.isApproved()) {
                    nl.addErrorMessage(NOT_APPROVED);
                }                
                else {                                           
                    loggedIn = u;
                    u.setLastLogin(LocalDateTime.now());
                    s.update(u);                                                
                }
            }            
        }
        s.getTransaction().commit();
        s.close();
        if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";
        }
        if (loggedIn.isAdministrator())
            return "/admin/admin";
        else {
            checkReservations();
            broadcastMessages();
            return "/user/user";                                                          
        }
    }
    
    public void checkReservations() {    
        if (loggedIn.bannedFromMakingReservations())
            return;
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        List<Reservation> reservations = s.createQuery("from Reservation where userID = :userID and approved = 0")
                .setParameter("userID", loggedIn.getUserID())
                .getResultList();        
        for (Reservation r: reservations) {
            if (r.isDenied())
                continue;
            if (ChronoUnit.DAYS.between(r.getMadeOn(), LocalDateTime.now()) >= 2) {
                r.setDenied(true);
                loggedIn.setFailedReservations(loggedIn.getFailedReservations() + 1);                
                Message m = new Message("Istekla vam je rezervacija!", loggedIn.getUserID());
                s.update(r);
                s.update(loggedIn);
                s.save(m);
                
                if (loggedIn.getFailedReservations() == 3) {
                    Message banned = new Message("Zabranjeno vam je da rezervišete", loggedIn.getUserID());
                    s.save(banned);
                }                
            }                        
        }
                
        s.getTransaction().commit();
        s.close();
    }
    
    public void broadcastMessages() {
         Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        List<Message> messages = s.createQuery("from Message where userID = :userID and shown = 0")
                .setParameter("userID", loggedIn.getUserID())
                .getResultList();
        
        NotificationList nl = new NotificationList();
        
        for (Message m: messages) {
            m.setShown(true);
            nl.addErrorMessage(m.getMessage());
            s.update(m);
        }
        
        s.getTransaction().commit();
        s.close();
        
        nl.printOutMessages();                            
    }
    
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/index";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(User loggedIn) {
        this.loggedIn = loggedIn;
    }
}
