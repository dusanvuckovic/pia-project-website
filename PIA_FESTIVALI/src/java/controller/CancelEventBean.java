package controller;

import hibernate.HibernateUtility;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Festival;
import model.FestivalTicketsPerDay;
import model.Media;
import model.Message;
import model.Performance;
import model.Rating;
import model.Reservation;
import org.hibernate.Session;

@ManagedBean
@ViewScoped
public class CancelEventBean {

    private List<Festival> festivals;
    private Integer festivalID;
    
    public CancelEventBean() {
        Session s = HibernateUtility.getSessionFactory().getCurrentSession();
        s.beginTransaction();

        festivals = s.createQuery("from Festival where currentStep = 0").getResultList();

        s.getTransaction().commit();
        s.close();
    }
    
    public String cancelFestival() {
        Festival f = null;
        for (Festival fest: festivals)
            if (fest.getFestivalID() == festivalID) {
                f = fest;
                break;
            }
        if (f == null)
            return "";                
        
        Session s = HibernateUtility.getSessionFactory().getCurrentSession();
        s.beginTransaction();
        
        List<Media> media = s.createQuery("from Media where festivalID = :festivalID")
                .setParameter("festivalID", festivalID)
                .getResultList();
        List<FestivalTicketsPerDay> tickets = s.createQuery("from FestivalTicketsPerDay where festivalID = :festivalID")
                .setParameter("festivalID", festivalID)
                .getResultList();
        List<Performance> performances = s.createQuery("from Performance where festivalID = :festivalID")
                .setParameter("festivalID", festivalID)
                .getResultList();
        List<Reservation> reservations = s.createQuery("from Reservation where festivalID = :festivalID and denied = 0")
                .setParameter("festivalID", festivalID)
                .getResultList();
        List<Rating> ratings = s.createQuery("from Rating where festivalID = :festivalID")
                .setParameter("festivalID", festivalID)
                .getResultList();
        
        for (Reservation r: reservations) {
            String festivalMessage = "Festival" + f.getFestivalLabel() + "je otkazan!";
            if (r.isApproved())
                festivalMessage += " Biće vam vraćen novac!";
            else
                festivalMessage += " Vaša rezervacija je otkazana!";
            Message m = new Message(festivalMessage, festivalID);
            s.save(m);
        }
        for (Media m: media) 
            s.delete(m);
        for (FestivalTicketsPerDay ftpd: tickets)
            s.delete(ftpd);
        for (Performance p: performances)
            s.delete(p);
        for (Rating r: ratings)
            s.delete(r);
        s.delete(f);
        
        s.getTransaction().commit();
        s.close();
        
        return "admin";
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }

    public Integer getFestivalID() {
        return festivalID;
    }

    public void setFestivalID(Integer festivalID) {
        this.festivalID = festivalID;
    }
    
    
   
    
}
