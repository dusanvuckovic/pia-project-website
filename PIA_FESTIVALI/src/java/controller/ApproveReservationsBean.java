package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Festival;
import model.FestivalTicketsPerDay;
import model.Reservation;
import model.User;
import model.additional.ReservationH;
import org.hibernate.Session;
import utility.NotificationList;

@ManagedBean
@RequestScoped
public class ApproveReservationsBean implements Serializable {

    private final List<String> options;
    private final String AS_IS = "Ignoriši",
            APPROVE = "Prihvati",
            DENY = "Odbij";

    private List<ReservationH> reservations;

    public ApproveReservationsBean() {
        Session s = HibernateUtility.getSessionFactory().getCurrentSession();
        s.beginTransaction();

        reservations = new ArrayList();

        List<Reservation> rs = s.createQuery("from Reservation where denied = 0 and approved = 0").getResultList();
        for (Reservation r : rs) {
            String username = ((User) s.createQuery("from User where userID = :userID")
                    .setParameter("userID", r.getUserID()).uniqueResult()).getUsername();
            Festival f = (Festival) s.createQuery("from Festival where festivalID = :festivalID")
                    .setParameter("festivalID", r.getFestivalID())
                    .uniqueResult();
            reservations.add(new ReservationH(r, username, f.getFestivalTitle(), Integer.toString(f.getFestivalYear())));
        }

        s.getTransaction().commit();
        s.close();

        options = new ArrayList();
        options.add(AS_IS);
        options.add(APPROVE);
        options.add(DENY);
    }
    
    public void approveDB(Session s, Reservation r) {                
        Festival f = (Festival) s.createQuery("from Festival where festivalID = :festivalID")
                    .setParameter("festivalID", r.getFestivalID())
                    .uniqueResult();
        if (r.getReservationDay() == 0) {
            f.setTicketSoldPackage(f.getTicketSoldPackage() + r.getNumberOfTickets());
            s.update(f);
        }
        else {
            FestivalTicketsPerDay ftpd = (FestivalTicketsPerDay) s.createQuery("from FestivalTicketsPerDay where festivalID = :festivalID and festivalDay = :festivalDay")
                    .setParameter("festivalID", f.getFestivalID())
                    .setParameter("festivalDay", r.getReservationDay())
                    .uniqueResult();
            ftpd.setTicketsRemaining(ftpd.getTicketsRemaining() - r.getNumberOfTickets());
            s.update(ftpd);        
        }    
    }

    public String approve() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        for (ReservationH res : reservations) {
            Reservation r = res.getR();
            switch (res.getOutcome()) {
                case AS_IS: break;
                case APPROVE: approveDB(s, r); r.setApproved(true); s.update(r); break;
                case DENY: r.setDenied(true); s.update(r); break;
            }
        }
        s.getTransaction().commit();
        s.close();
        
        NotificationList nl = new NotificationList();
        nl.addInfoMessage("Uspešno obrađene rezervacije!");
        nl.printOutMessages();
        return "admin";
    }

    public List<ReservationH> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationH> reservations) {
        this.reservations = reservations;
    }

    public List<String> getOptions() {
        return options;
    }

}
