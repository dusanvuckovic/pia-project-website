package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Festival;
import model.FestivalTicketsPerDay;
import model.Media;
import model.Performance;
import model.additional.PerformanceH;
import org.hibernate.Session;
import utility.Conversions;
import utility.NotificationList;

@ManagedBean
@ViewScoped
public class AddFestivalDataBean implements Serializable {

    private Date now;

    private Festival f;
    private Date festivalFrom;
    private Date festivalTo;

    private PerformanceH performance;
    private List<PerformanceH> performances;

    private final String NO_TITLE = "Morate uneti naslov festivala!",
            NO_PLACE = "Morate uneti mesto održavanja festivala!",
            EXISTS_TITLE_AND_PLACE = "Ovaj festival je već unet za ovu godinu!",
            NO_PERFORMER_NAME = "Morate uneti naziv izvođača!",
            TICKET_NUMBER_DAY_NEGATIVE = "Broj dnevnih karata mora biti veći od nule!",
            TICKET_NUMBER_PACKAGE_NEGATIVE = "Broj paketa karata mora biti veći od nule!",
            TICKET_PRICE_DAY_NEGATIVE = "Cena dnevne karte mora biti veća od nule!",
            TICKET_PRICE_PACKAGE_NEGATIVE = "Cena paketa karata mora biti veća od nule!",
            MAX_RESERVATIONS_NEGATIVE = "Maksimalni broj rezervacija ne sme biti negativan!",
            START_AFTER_END = "Početak festivala ne sme biti pre kraja festivala!",
            NO_PERFORMERS = "Morate imati barem jednog izvođača na festivalu!",
            PERFORMER_START_BEFORE_END = "Izvođač mora da započne koncert pre nego što završi koncert!",
            PERFORMER_BEYOND_FESTIVAL = " ne nastupa za vreme festivala!",
            PERFORMER_SCHEDULE_CLASH = "Izvođač ne može nastupati u isto vreme kad i ",
            PERFORMER_MAX_TIME = "Izvođač mora da nastupa najduže tri sata!",
            PERFORMER_DOUBLED = "Ovaj izvođač je već dodat u listu izvođača!";

    public AddFestivalDataBean() {
    }

    private void initializeNewFestival() {
        f = new Festival();
        now = Conversions.toDate(LocalDateTime.now());
        performance = new PerformanceH();
        performances = new ArrayList();
        festivalFrom = Date.from(Instant.now());
        festivalTo = Date.from(Instant.now());
    }

    private void initializeFestivalFromDB() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        List<Performance> perfs = s.createQuery("from Performance where festivalID = :festivalID")                
                .setParameter("festivalID", f.getFestivalID())
                .getResultList();
        performances = new ArrayList();
        for (Performance p : perfs) {
            performances.add(new PerformanceH(p));
        }
        performance = new PerformanceH();

        s.getTransaction().commit();
        s.close();
        
        festivalFrom = Conversions.toDate(f.getFestivalFrom());
        festivalTo = Conversions.toDate(f.getFestivalTo());
        
        if (festivalFrom.before(Date.from(Instant.now()))) {
            now = festivalFrom;
        }
        else
            now = Conversions.toDate(LocalDateTime.now());        
    }

    public boolean disable() {
        return true;
    }
    public boolean enable() {
        return false;
    }
    
    @PostConstruct
    public void init() {

        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        f = (Festival) s.createQuery("from Festival where currentStep <> 0").uniqueResult();

        s.getTransaction().commit();
        s.close();

        if (f == null)
            initializeNewFestival();
        else
            initializeFestivalFromDB();
    }
    
    private boolean checkIfFestivalExists(String festivalTitle, int festivalYear) {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        Festival f = (Festival) s.createQuery("from Festival where festivalTitle = :festivalTitle and festivalYear = :festivalYear and currentStep = 0")
                .setParameter("festivalTitle", festivalTitle)
                .setParameter("festivalYear", festivalYear)
                .uniqueResult();

        s.getTransaction().commit();
        s.close();

        return (f != null);

    }

    private NotificationList checkForErrors() {

        NotificationList nl = new NotificationList();

        if ("".equals(f.getFestivalTitle())) {
            nl.addErrorMessage(NO_TITLE);
        }
        if ("".equals(f.getPlace())) {
            nl.addErrorMessage(NO_PLACE);
        }
        if (f.getTicketNumberDay() <= 0) {
            nl.addErrorMessage(TICKET_NUMBER_DAY_NEGATIVE);
        }
        if (f.getTicketNumberPackage() <= 0) {
            nl.addErrorMessage(TICKET_NUMBER_PACKAGE_NEGATIVE);
        }
        if (f.getTicketPriceDay() <= 0) {
            nl.addErrorMessage(TICKET_PRICE_DAY_NEGATIVE);
        }
        if (f.getTicketPricePackage() <= 0) {
            nl.addErrorMessage(TICKET_PRICE_PACKAGE_NEGATIVE);
        }
        
        if (f.getMaxReservations() <= 0) {
            nl.addErrorMessage(MAX_RESERVATIONS_NEGATIVE);
        }

        if (performances.isEmpty()) {
            nl.addErrorMessage(NO_PERFORMERS);
        }

        if (f.getFestivalFrom().isAfter(f.getFestivalTo())) {
            nl.addErrorMessage(START_AFTER_END);
        }

        for (PerformanceH p : performances) {
            if (!isBetween(f.getFestivalFrom(), f.getFestivalTo(), p.getFrom(), p.getTo())) {
                nl.addErrorMessage(p.getPerformer() + PERFORMER_BEYOND_FESTIVAL);
            }
        }

        if (checkIfFestivalExists(f.getFestivalTitle(), f.getFestivalYear())) {
            nl.addErrorMessage(EXISTS_TITLE_AND_PLACE);
        }

        return nl;

    }

    private void updateFestival() {
        f.setFestivalFrom(Conversions.toLDT(festivalFrom));
        f.setFestivalTo(Conversions.toLDT(festivalTo));
        f.setFestivalYear(f.getFestivalTo().getYear());
    }

    public String goToStep(int step) {
        updateFestival();
        NotificationList nl = checkForErrors();
        if (nl.printOutMessagesIfError())
            return "";        
        
        saveToDatabase(f);
        
        if (step == 2)
            return "addnewfestivalmedia";
        else if (step == 3)
            return "addnewfestivaloverview";          
        return "";
    }

    public String process() {

        updateFestival();
        NotificationList nl = checkForErrors();
        if (nl.printOutMessagesIfError()) {
            return "";
        }

        saveToDatabase(f);

        return "addnewfestivalmedia";
    }

    private void saveToDatabase(Festival f) {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        Integer ID;
        if (!f.isInDB()) {
            ID = (Integer) s.save(f);
        }
        else {
            s.update(f);
            ID = f.getFestivalID();   
            s.createQuery("delete from FestivalTicketsPerDay where festivalID = :festivalID")
                    .setParameter("festivalID", ID).executeUpdate();                
            s.createQuery("delete from Performance where festivalID = :festivalID")
                    .setParameter("festivalID", ID).executeUpdate();
        }
        
        for (int i = 0; i < f.getNumberOfDays(); i++) {
            FestivalTicketsPerDay ft = new FestivalTicketsPerDay(ID, i + 1, f.getTicketNumberDay());
            s.save(ft);
        }
        for (PerformanceH p : performances) {
            Performance pDB = new Performance(f, p);
            s.save(pDB);
        }
        s.getTransaction().commit();
        s.close();
    }

    private boolean isBetween(LocalDateTime from, LocalDateTime to, LocalDateTime from_t, LocalDateTime to_t) {
        boolean tComesAfter = from_t.isAfter(to),
                tComesBefore = to_t.isBefore(from);
        return !(tComesAfter || tComesBefore);
    }

    private boolean isBetween(LocalDateTime from, LocalDateTime to, Date from_t, Date to_t) {
        LocalDateTime from_t_ldt = Conversions.toLDT(from_t);
        LocalDateTime to_t_ldt = Conversions.toLDT(to_t);
        return isBetween(from, to, from_t_ldt, to_t_ldt);
    }

    public void addPerformer() {

        NotificationList nl = new NotificationList();

        if ("".equals(performance.getPerformer())) {
            nl.addErrorMessage(NO_PERFORMER_NAME);
        }
        if (nl.printOutMessagesIfError()) {
            return;
        }

        for (PerformanceH p : performances) {
            if (performance.equals(p)) {
                nl.addErrorMessage(PERFORMER_DOUBLED);
            }
        }
        if (nl.printOutMessagesIfError()) {
            return;
        }

        LocalDateTime from = Conversions.toLDT(performance.getFrom()),
                to = Conversions.toLDT(performance.getTo());
        if (!from.isBefore(to)) {
            nl.addErrorMessage(PERFORMER_START_BEFORE_END);
        }
        if (to.minusHours(3).isAfter(from)) {
            nl.addErrorMessage(PERFORMER_MAX_TIME);
        }
        for (PerformanceH p : performances) {
            if (isBetween(from, to, p.getFrom(), p.getTo())) {
                nl.addErrorMessage(PERFORMER_SCHEDULE_CLASH + p.getPerformer() + "!");
                break;
            }
        }

        if (nl.printOutMessagesIfError()) {
            return;
        }

        performances.add(performance);
        Collections.sort(performances);
        performance = new PerformanceH();

    }

    public void removePerformer(int id) {
        PerformanceH p = null;
        for (PerformanceH psearch : performances) {
            if (psearch.getID() == id) {
                p = psearch;
                break;
            }
        }
        performances.remove(p);
    }

    public void clearPerformers() {
        performances = new ArrayList();
    }

    public Date getFestivalFrom() {
        return festivalFrom;
    }

    public void setFestivalFrom(Date festivalFrom) {
        this.festivalFrom = festivalFrom;
    }

    public Date getFestivalTo() {
        return festivalTo;
    }

    public void setFestivalTo(Date festivalTo) {
        this.festivalTo = festivalTo;
    }

    public Date getNow() {
        return now;
    }

    public void setNow(Date now) {
        this.now = now;
    }

    public List<PerformanceH> getPerformances() {
        return performances;
    }

    public void setPerformances(List<PerformanceH> performances) {
        this.performances = performances;
    }

    public PerformanceH getPerformance() {
        return performance;
    }

    public void setPerformance(PerformanceH performance) {
        this.performance = performance;
    }

    public Festival getF() {
        return f;
    }

    public void setF(Festival f) {
        this.f = f;
    }

}
