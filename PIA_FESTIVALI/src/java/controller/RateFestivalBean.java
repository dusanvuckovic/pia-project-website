package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import model.Festival;
import model.Media;
import model.Rating;
import model.User;
import model.additional.MediaH;
import org.hibernate.Session;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import utility.MediaCacher;

@ManagedBean
@ViewScoped
public class RateFestivalBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loggedIn;

    private List<Festival> festivals;
    private Integer selectedFestivalID;
    private Integer rating;
    private String comment;

    private List<MediaH> media;

    public RateFestivalBean() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        festivals = s.createQuery("from Festival where currentStep = 0").getResultList();
        if (!festivals.isEmpty()) {
            selectedFestivalID = festivals.get(0).getFestivalID();
        }

        s.getTransaction().commit();
        s.close();

        rating = 0;
        comment = "";
        media = new ArrayList();

    }

    public void clearImagesFromGallery() {
        for (MediaH med : media) {
            switch (med.getState()) {
                case NEW_ADD_TO:
                    med.setState(MediaH.MediaEnum.NEW_CANCELLED);
                    break;
                case DB_AS_IS:
                    med.setState(MediaH.MediaEnum.DB_DELETE);
                    break;
                default:
                    break;
            }
        }
    }

    public String rateFestival() {
        User u = loggedIn.getLoggedIn();
        Festival f = null;
        for (Festival fs : festivals) {
            if (fs.getFestivalID() == selectedFestivalID) {
                f = fs;
                break;
            }
        }
        if (f == null) {
            return "";
        }

        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        Rating r = (Rating) s.createQuery("from Rating where festivalID = :festivalID and userID = :userID")
                .setParameter("festivalID", f.getFestivalID())
                .setParameter("userID", u.getUserID())
                .uniqueResult();

        if (r == null) {
            r = new Rating(u.getUserID(), u.getUsername(), f.getFestivalID(), f.getFestivalTitle(), rating, comment);
        }

        s.saveOrUpdate(r);

        saveMedia(f);

        s.getTransaction().commit();
        s.close();

        return "user";

    }

    public void handleFileUpload(FileUploadEvent event) {

        UploadedFile file = event.getFile();

        Media newMedia = new Media(0, 0, file.getFileName(), file.getContentType(), file.getContents(), false);

        MediaH uploadedMedia = null;
        for (MediaH mediaElement : media) {
            if (mediaElement.getM().isSameImage(newMedia)) {
                uploadedMedia = mediaElement;
                break;
            }
        }

        if (uploadedMedia == null) {
            String saveLocation = MediaCacher.saveToLocation(newMedia);
            uploadedMedia = new MediaH(newMedia, saveLocation, MediaH.MediaEnum.NEW_ADD_TO);
            media.add(uploadedMedia);
        } else {
            if (uploadedMedia.getState() == MediaH.MediaEnum.DB_DELETE) {
                uploadedMedia.setState(MediaH.MediaEnum.DB_AS_IS);
            } else if (uploadedMedia.getState() == MediaH.MediaEnum.NEW_CANCELLED) {
                uploadedMedia.setState(MediaH.MediaEnum.NEW_ADD_TO);
            }
        }

    }

    public List<MediaH> getImagesToRender() {
        List<MediaH> list = new ArrayList<>();
        for (MediaH mh : media) {
            if (mh.shouldRender()) {
                list.add(mh);
            }
        }
        return list;
    }

    public boolean renderImages() {
        for (MediaH mediaH : media) {
            if (mediaH.getState() == MediaH.MediaEnum.DB_AS_IS) {
                return true;
            }
            if (mediaH.getState() == MediaH.MediaEnum.NEW_ADD_TO) {
                return true;
            }
        }
        return false;
    }

    private void saveMedia(Festival f) {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        for (MediaH med : media) {
            med.getM().setFestivalID(f.getFestivalID());
            switch (med.getState()) {
                case DB_DELETE:
                    s.delete(med.getM());
                    break;
                case NEW_ADD_TO:
                    s.save(med.getM());
                    break;
                default:
                    break;
            }
        }
        s.getTransaction().commit();
        s.close();
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }

    public Integer getSelectedFestivalID() {
        return selectedFestivalID;
    }

    public void setSelectedFestivalID(Integer selectedFestivalID) {
        this.selectedFestivalID = selectedFestivalID;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LoginBean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(LoginBean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public List<MediaH> getMedia() {
        return media;
    }

    public void setMedia(List<MediaH> media) {
        this.media = media;
    }

}
