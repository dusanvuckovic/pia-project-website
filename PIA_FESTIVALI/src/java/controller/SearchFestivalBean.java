package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import model.Festival;
import model.Performance;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import utility.Conversions;

@ManagedBean
@ViewScoped
public class SearchFestivalBean implements Serializable {


    
    private List<Festival> festivals;
    
    private String festivalName;
    private Date festivalFrom;
    private Date festivalTo;
    private String festivalPlace;
    private String festivalPerformer;
    private List<String> festivalPerformers;
    
    
    public SearchFestivalBean() {
        festivalPerformers = new ArrayList();
        festivalFrom = Conversions.toDate((LocalDateTime.of(2014, Month.JANUARY, 1, 0, 0, 0)));
        festivalTo = Conversions.toDate((LocalDateTime.of(2018, Month.FEBRUARY, 1, 0, 0, 0)));
    }
    
    public void addToPerformers() {
        if (!festivalPerformers.contains(festivalPerformer))
            festivalPerformers.add(festivalPerformer);
    }
    public void clearPerformers() {
        festivalPerformers.clear();        
    }
    
    public void removePerformer(String performer) {
        festivalPerformers.remove(performer);
    }
    
    public boolean performerInCollection(String findInCollection) {
        for (String p: festivalPerformers)
            if (p.equals(findInCollection))
                return true;
        return false;
    }
    
    public boolean everyPerformerInCollection(List<Performance> pList) {        
        if (festivalPerformers.isEmpty())
            return true;
        for (Performance p: pList)
            if (!performerInCollection(p.getPerformer()))
                return false;
        return true;
    }
    
    public String searchFestivals() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        LocalDateTime to = Conversions.toLDT(festivalTo),
                      from = Conversions.toLDT(festivalFrom);
        
        Criteria c = s.createCriteria(Festival.class);
        if (!"".equals(festivalName))
            c.add(Restrictions.like("festivalTitle", festivalName, MatchMode.ANYWHERE));
        SimpleExpression searchEndBeforeBeginning = Restrictions.ge("festivalFrom", to);
        SimpleExpression searchBeginAfterEnding = Restrictions.lt("festivalTo", from);
        Criterion between = Restrictions.not(Restrictions.or(searchEndBeforeBeginning, searchBeginAfterEnding));
        c.add(between);
        c.add(Restrictions.ge("festivalFrom", LocalDateTime.now()));
        if (!"".equals(festivalPlace))
            c.add(Restrictions.like("place", festivalPlace, MatchMode.ANYWHERE));             
                
        festivals = c.list();
        List<Festival> trueFestivals = new ArrayList<>();
        for (Festival f: festivals) {
            List<Performance> performances = s.createQuery("from Performance where festivalID = :festivalID")
                    .setParameter("festivalID", f.getFestivalID())
                    .getResultList();
            if (everyPerformerInCollection(performances))
                trueFestivals.add(f);
        }        
        festivals = trueFestivals;
        for (Festival festival: festivals) {
            festival.setRenderSearch(true);
            s.update(festival);
        }
        
        s.getTransaction().commit();
        s.close();
        
        return "displayfestivals";
    }
    
    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public Date getFestivalFrom() {
        return festivalFrom;
    }

    public void setFestivalFrom(Date festivalFrom) {
        this.festivalFrom = festivalFrom;
    }

    public Date getFestivalTo() {
        return festivalTo;
    }

    public void setFestivalTo(Date festivalTo) {
        this.festivalTo = festivalTo;
    }

    public String getFestivalPlace() {
        return festivalPlace;
    }

    public void setFestivalPlace(String festivalPlace) {
        this.festivalPlace = festivalPlace;
    }

    public String getFestivalPerformer() {
        return festivalPerformer;
    }

    public void setFestivalPerformer(String festivalPerformer) {
        this.festivalPerformer = festivalPerformer;
    }

    public List<String> getFestivalPerformers() {
        return festivalPerformers;
    }

    public void setFestivalPerformers(List<String> festivalPerformers) {
        this.festivalPerformers = festivalPerformers;
    }            

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }        
}
