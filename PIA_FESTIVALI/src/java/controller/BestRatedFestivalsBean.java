package controller;

import hibernate.HibernateUtility;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Festival;
import model.Rating;
import model.additional.FestivalH;
import org.hibernate.Session;

@ManagedBean
@RequestScoped
public class BestRatedFestivalsBean {
           
    private List<FestivalH> bestRatedFestivals;
    
    public double getRanking(Festival f, Session s) {
        List<Rating> ratings = s.createQuery("from Rating where festivalID = :festivalID")
                .setParameter("festivalID", f.getFestivalID())
                .getResultList();
        if (ratings.isEmpty())
            return 0;
        double sum = 0;
        for (Rating r: ratings)
            sum += r.getRating();
        sum /= ratings.size();
        return sum;
    }
    
           
    public BestRatedFestivalsBean() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        List<Festival> festivals = s.createQuery("from Festival where currentStep = 0").getResultList();
        bestRatedFestivals = new ArrayList();
        for (Festival f: festivals)
            bestRatedFestivals.add(new FestivalH(f, getRanking(f, s)));
        Collections.sort(bestRatedFestivals);
        bestRatedFestivals = bestRatedFestivals.subList(0, Math.min(bestRatedFestivals.size(), 5));                    
        
        s.getTransaction().commit();
        s.close();
        
    }

    public List<FestivalH> getBestRatedFestivals() {
        return bestRatedFestivals;
    }

    public void setBestRatedFestivals(List<FestivalH> bestRatedFestivals) {
        this.bestRatedFestivals = bestRatedFestivals;
    }
    
    
    
}
