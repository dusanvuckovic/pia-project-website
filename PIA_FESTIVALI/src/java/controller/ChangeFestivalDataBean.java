package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Festival;
import org.hibernate.Session;
import utility.NotificationList;

@ManagedBean
@ViewScoped
public class ChangeFestivalDataBean implements Serializable {

    private List<Festival> festivals;
    private Integer selectedFestival;    

    public ChangeFestivalDataBean() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        festivals = s.createQuery("from Festival where currentStep = 0").getResultList();
        if (!festivals.isEmpty()) {
            selectedFestival = festivals.get(0).getFestivalID();
        }

        s.getTransaction().commit();
        s.close();
    }

    private boolean allFestivalsProcessed() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        Festival f = (Festival) s.createQuery("from Festival where currentStep <> 0").uniqueResult();

        s.getTransaction().commit();
        s.close();

        return (f == null);

    }

    public String change() {

        NotificationList nl = new NotificationList();
        if (!allFestivalsProcessed()) {
            nl.addErrorMessage("Morate uneti festival na čekanju pre menjanja postojećeg!");
        }
        if (nl.printOutMessagesIfError()) {
            return "";
        }
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        Festival f = (Festival) s.createQuery("from Festival where festivalID = :festivalID").setParameter("festivalID", selectedFestival).uniqueResult();

        s.getTransaction().commit();
        s.close();
        
        f.setCurrentStep(2);
        HibernateUtility.updateObjectInDB(f);
        return "addnewfestivaloverview";
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }

    public Integer getSelectedFestival() {
        return selectedFestival;
    }

    public void setSelectedFestival(Integer selectedFestival) {
        this.selectedFestival = selectedFestival;
    }        

   
}
