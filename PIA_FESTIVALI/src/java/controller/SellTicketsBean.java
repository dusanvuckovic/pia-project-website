package controller;

import hibernate.HibernateUtility;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Festival;
import model.FestivalTicketsPerDay;
import org.hibernate.Session;
import utility.NotificationList;

@ManagedBean
@ViewScoped
public class SellTicketsBean {

    private final String BAD_TICKET_NUMBER = "Broj odabranih karata mora biti veći od nule!",
            NO_TICKETS_REMAINING_DAY = "Više nema karata za ovaj dan festivala!",
            NO_TICKETS_REMAINING_PACKAGE = "Više nema paketa karata za ceo festival",
            TOO_FEW_TICKETS_REMAINING_PACKAGE_1 = "Ostalo je samo ",
            TOO_FEW_TICKETS_REMAINING_PACKAGE_2 = " paketa karata!",
            TOO_FEW_TICKETS_REMAINING_DAY_1 = "Ostalo je samo ",
            TOO_FEW_TICKETS_REMAINING_DAY_2 = " dnevnih karata za dan ";

    private List<Festival> festivals;
    private Integer selectedFestivalID;
    private List<Integer> festivalDays;

    private Integer selectedDay;
    private boolean entireFestival;
    private Integer numberOfTickets;

    @PostConstruct
    public void initialize() {
        Session s = hibernate.HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        festivals = s.createQuery("from Festival where currentStep = 0").getResultList();
        if (!festivals.isEmpty()) {
            selectedFestivalID = festivals.get(0).getFestivalID();
            onFestivalSelectionChange();
        }

        s.getTransaction().commit();
        s.close();

        numberOfTickets = 0;
        entireFestival = false;

    }

    public boolean renderForm() {
        return !festivals.isEmpty();
    }

    public SellTicketsBean() {

    }

    public void onFestivalSelectionChange() {
        Session s = hibernate.HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        Festival f = (Festival) s.createQuery("from Festival where festivalID = :festivalID")
                .setParameter("festivalID", selectedFestivalID)
                .uniqueResult();

        s.getTransaction().commit();
        s.close();

        festivalDays = new ArrayList();
        for (int i = 1; i <= f.getNumberOfDays(); i++) {
            festivalDays.add(i);
        }
        selectedDay = festivalDays.get(0);
    }

    public String sellTickets() {
        NotificationList nl = new NotificationList();
        if (!entireFestival && numberOfTickets <= 0) {
            nl.addErrorMessage(BAD_TICKET_NUMBER);
        }

        if (nl.printOutMessagesIfError()) {
            return "";
        }

        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        Festival f = (Festival) s.createQuery("from Festival where festivalID = :festivalID")
                .setParameter("festivalID", selectedFestivalID)
                .uniqueResult();
        if (entireFestival) {
            int packageTickets = f.getTicketNumberPackage() - f.getTicketSoldPackage();
            if (packageTickets == 0) {
                nl.addErrorMessage(NO_TICKETS_REMAINING_PACKAGE);
            } else if (packageTickets < numberOfTickets) {
                nl.addErrorMessage(TOO_FEW_TICKETS_REMAINING_PACKAGE_1 + Integer.toString(packageTickets) + TOO_FEW_TICKETS_REMAINING_PACKAGE_2);
            } else {
                packageTickets -= numberOfTickets;
                f.setTicketSoldPackage(packageTickets);
                s.save(f);
                nl.addInfoMessage("Uspešno kupljen paket karata!");
            }
        } else {
            FestivalTicketsPerDay td = (FestivalTicketsPerDay) s.createQuery("from FestivalTicketsPerDay where festivalID = :festivalID and festivalDay = :festivalDay")
                    .setParameter("festivalID", f.getFestivalID())
                    .setParameter("festivalDay", selectedDay)
                    .uniqueResult();
            int ticketsRemaining = td.getTicketsRemaining();
            if (ticketsRemaining == 0) {
                nl.addErrorMessage(NO_TICKETS_REMAINING_DAY);
            } else if (ticketsRemaining < numberOfTickets) {
                nl.addErrorMessage(TOO_FEW_TICKETS_REMAINING_DAY_1 + Integer.toString(ticketsRemaining)
                        + TOO_FEW_TICKETS_REMAINING_DAY_2 + Integer.toString(selectedDay) + "!");
            } else {
                ticketsRemaining -= numberOfTickets;
                td.setTicketsRemaining(ticketsRemaining);
                s.save(td);
                nl.addInfoMessage("Uspešno kupljene dnevne karte!");
            }
        }

        s.getTransaction().commit();
        s.close();

        if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";
        } else {
            nl.printOutMessages();
            return "admin";
        }

    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }

    public Integer getSelectedFestivalID() {
        return selectedFestivalID;
    }

    public void setSelectedFestivalID(Integer selectedFestivalID) {
        this.selectedFestivalID = selectedFestivalID;
    }

    public List<Integer> getFestivalDays() {
        return festivalDays;
    }

    public void setFestivalDays(List<Integer> festivalDays) {
        this.festivalDays = festivalDays;
    }

    public Integer getSelectedDay() {
        return selectedDay;
    }

    public void setSelectedDay(Integer selectedDay) {
        this.selectedDay = selectedDay;
    }

    public Integer getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(Integer numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public boolean isEntireFestival() {
        return entireFestival;
    }

    public void setEntireFestival(boolean entireFestival) {
        this.entireFestival = entireFestival;
    }

}
