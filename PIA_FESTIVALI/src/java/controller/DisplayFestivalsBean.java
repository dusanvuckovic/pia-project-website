package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Festival;
import org.hibernate.Session;

@ManagedBean
@ViewScoped
public class DisplayFestivalsBean implements Serializable {
    
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loggedIn;
    
    private List<Festival> festivals;
    
    public DisplayFestivalsBean() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        festivals = s.createQuery("from Festival where renderSearch = 1").getResultList();
        for (Festival f : festivals) {
            f.setRenderSearch(false);
            s.update(f);
        }
        
        s.getTransaction().commit();
        s.close();
    }
    
    public boolean disableButton() {
        return loggedIn.getLoggedIn() == null;
    }
    
    public String returnFromFestival() {
        return (loggedIn.getLoggedIn() == null) ? "/index" : "/user/user";
    }
    
    public String showDetails(int festivalID) {        
        for (Festival fSearch: festivals) {
            if (fSearch.getFestivalID() == festivalID) {
                fSearch.setRenderSelect(true);
                HibernateUtility.updateObjectInDB(fSearch);
                break;
            }
        }
        return "festivaldetails";
    }
    
    public String reserveTickets(int festivalID) {
        for (Festival fSearch: festivals) {
            if (fSearch.getFestivalID() == festivalID) {
                fSearch.setRenderReservation(true);
                HibernateUtility.updateObjectInDB(fSearch);
                break;
            }
        }
        return "makereservations";
    }
    
    public List<Festival> getFestivals() {
        return festivals;
    }
    
    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }

    public LoginBean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(LoginBean loggedIn) {
        this.loggedIn = loggedIn;
    }
    
    
    
    
    
}
