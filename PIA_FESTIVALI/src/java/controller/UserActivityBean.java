package controller;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;

@ManagedBean
@RequestScoped
public class UserActivityBean implements Serializable {

    private List<User> top10Users;
    
    public UserActivityBean() {
        Session s = hibernate.HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();

        Query q = s.createQuery("from User order by lastLogin desc");
        q.setMaxResults(10);
        top10Users = q.getResultList();

        s.getTransaction().commit();
        s.close();
    }

    public List<User> getTop10Users() {
        return top10Users;
    }

    public void setTop10Users(List<User> top10Users) {
        this.top10Users = top10Users;
    }
    
    
    
}
