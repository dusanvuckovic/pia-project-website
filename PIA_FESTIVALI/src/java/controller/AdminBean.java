import hibernate.HibernateUtility;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import model.Festival;
import org.hibernate.Query;
import org.hibernate.Session;

@ManagedBean
@RequestScoped
public class AdminBean implements Serializable {
    
    private List<Festival> top5Views;
    private List<Festival> top5TicketsSold;

    public AdminBean() {
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        Query q1 = s.createQuery("from Festival order by viewCount desc");
        q1.setMaxResults(5);
        top5Views = q1.getResultList();
        
        Query q2 = s.createQuery("from Festival order by ticketSoldPackage desc");
        q2.setMaxResults(5);
        top5TicketsSold = q2.getResultList();
        
        s.getTransaction().commit();
        s.close();
    }

    public List<Festival> getTop5Views() {
        return top5Views;
    }

    public void setTop5Views(List<Festival> top5Views) {
        this.top5Views = top5Views;
    }

    public List<Festival> getTop5TicketsSold() {
        return top5TicketsSold;
    }

    public void setTop5TicketsSold(List<Festival> top5TicketsSold) {
        this.top5TicketsSold = top5TicketsSold;
    }
    
    
    
    
    
}
