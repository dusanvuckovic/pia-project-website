package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.User;
import org.hibernate.Session;
import utility.NotificationList;


@ManagedBean
@RequestScoped
public class AccountManagementBean implements Serializable {
    
    public class Account {
        private User user;
        private String status;

        public Account() {
        }              
        
        public Account(User u, String s) {
            this.user = u;
            this.status = s;
        }
        
        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }                                
    
    }
    
    private static final String WAITING = "Na čekanju",
                                APPROVE = "Odobri",
                                DENY    = "Zabrani";
    private List<Account> accounts;      
    
    @PostConstruct
    public void initialize() {    
        accounts = new ArrayList();
        
        List<User> usersToApprove;
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        usersToApprove = s.createQuery("from User u where u.administrator = false AND u.approved = false AND u.denied = false").getResultList();
        s.close();                                       
        
        for (int i = 0; i < usersToApprove.size(); i++)
            accounts.add(new Account(usersToApprove.get(i), WAITING));
                                                              
    }

    public AccountManagementBean() {     
    }
    
    public String manage() {        
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        for (Account a: accounts) {
            User u = a.getUser();
            String value = a.getStatus();
            switch (value) {
                case APPROVE: 
                    u.setApproved(true); 
                    s.update(u); 
                    break;
                case DENY:    
                    u.setDenied(true); 
                    s.update(u); 
                    break;            
                default: break;
            }
        }       
        s.getTransaction().commit();
        s.close();
        new NotificationList().addInfoMessage("Uspešno načinjene promene nad nalozima!").printOutMessages();        
        return "/admin/admin";
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }       
    
    
}
