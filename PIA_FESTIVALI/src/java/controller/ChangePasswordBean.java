package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.User;
import utility.Checks;
import utility.NotificationList;

@ManagedBean
@RequestScoped
public class ChangePasswordBean implements Serializable {
    
    private final String NO_ORIGINAL_PASSWORD = "Niste uneli vašu lozinku!",
                         NO_NEW_PASSWORD = "Niste uneli novu lozinku!",
                         NO_REPEATED_PASSWORD = "Niste uneli ponovljenu lozinku!",
                         PASSWORD_MISMATCH = "Ponovljena lozinka nije ista kao željena nova lozinka!",
                         WRONG_INITIAL_PASSWORD = "Niste tačno uneli svoju lozinku!",
                         INVALID_PASSWORD_FORMAT = "Pogrešan format lozinke!",
                         SUCCESSFUL_PASS_CHANGE = "Uspešno ste izmenili svoju lozinku!";
    
    @ManagedProperty(value="#{loginBean}")
    private LoginBean loggedIn;
    
    private String passwordOriginal = "",
                   passwordNew = "",
                   passwordRepeated = "";
                                
    
    public ChangePasswordBean() {}        
    
    public String changePassword() {            
        NotificationList nl = new NotificationList();
        User logged = loggedIn.getLoggedIn();
        
        if ("".equals(passwordOriginal))
            nl.addErrorMessage(NO_ORIGINAL_PASSWORD);
        if ("".equals(passwordNew))
            nl.addErrorMessage(NO_NEW_PASSWORD);
        if ("".equals(passwordRepeated))
            nl.addErrorMessage(NO_REPEATED_PASSWORD); 
         if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";
        }
         
        if (!passwordNew.equals(passwordRepeated))
            nl.addErrorMessage(PASSWORD_MISMATCH);
        
        //if (Checks.isPasswordOKBool(passwordNew))
          //  nl.addErrorMessage(INVALID_PASSWORD_FORMAT);
        
        if (!logged.getPassword().equals(passwordOriginal))
            nl.addErrorMessage(WRONG_INITIAL_PASSWORD);
        if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";
        }
        
        logged.setPassword(passwordNew);
        HibernateUtility.updateObjectInDB(logged);                            
        nl.addInfoMessage(SUCCESSFUL_PASS_CHANGE);
        nl.printOutMessages();  
        return back();
    }
    
    public String back() {
        if (loggedIn.getLoggedIn().isAdministrator())
            return "/admin/admin";
        else
            return "/user/user";                
    }

    public LoginBean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(LoginBean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getPasswordOriginal() {
        return passwordOriginal;
    }

    public void setPasswordOriginal(String passwordOriginal) {
        this.passwordOriginal = passwordOriginal;
    }

    public String getPasswordNew() {
        return passwordNew;
    }

    public void setPasswordNew(String passwordNew) {
        this.passwordNew = passwordNew;
    }

    public String getPasswordRepeated() {
        return passwordRepeated;
    }

    public void setPasswordRepeated(String passwordRepeated) {
        this.passwordRepeated = passwordRepeated;
    }
    
    
    
    
}
