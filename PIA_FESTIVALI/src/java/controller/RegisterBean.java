package controller;

import hibernate.HibernateUtility;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.User;
import org.hibernate.Session;
import utility.Checks;
import utility.NotificationList;

@ManagedBean
@RequestScoped
public class RegisterBean implements Serializable {
    
        private final String USERNAME_EMPTY = "Morate uneti korisničko ime!",
                             PASSWORD_EMPTY = "Morate uneti lozinku!",
                             PASSWORD_R_EMPTY = "Morate uneti ponovljenu lozinku!",
                             FIRSTNAME_EMPTY = "Morate uneti svoje ime!",
                             LASTNAME_EMPTY = "Morate uneti svoje prezime!",
                             PHONE_EMPTY = "Morate uneti svoj broj telefona!",
                             EMAIL_EMPTY = "Morate uneti svoju elektronsku poštu!",
                             USERNAME_TAKEN = "Ovo korisničko ime je već zauzeto; morate odabrati drugo!",
                             PASSWORDS_NOT_ALIKE = "Vaša ponovljena lozinka se ne podudara sa originalnom!",
                             PHONE_WRONG = "Morate uneti validni broj telefona!",
                             EMAIL_WRONG = "Morate uneti validnu adresu elektronske pošte!",
                             SUCCESS = "Uspešno ste se registrovali; sačekajte da vas administrator odobri!";    
    
        private String username;
        private String password;        
        private String passwordRepeated;
        private String firstName;
        private String lastName;
        private String phone;
        private String email;
        
    public RegisterBean() {}                                                 
    
    public String register() {
        NotificationList nl = new NotificationList();
        
        //empty checks
        if ("".equals(username))       
            nl.addErrorMessage(USERNAME_EMPTY);
        if ("".equals(password))       
            nl.addErrorMessage(PASSWORD_EMPTY);
        if ("".equals(passwordRepeated))       
            nl.addErrorMessage(PASSWORD_R_EMPTY);
        if ("".equals(firstName))       
            nl.addErrorMessage(FIRSTNAME_EMPTY);
        if ("".equals(lastName))       
            nl.addErrorMessage(LASTNAME_EMPTY);
        if ("".equals(phone))       
            nl.addErrorMessage(PHONE_EMPTY);
        if ("".equals(email))       
            nl.addErrorMessage(EMAIL_EMPTY);
        if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";            
        }
        
        //validity checks
        if (!Checks.isPhoneValid(phone))
            nl.addErrorMessage(PHONE_WRONG);
        if (!Checks.isEmailValid(email))
            nl.addErrorMessage(EMAIL_WRONG);
        if (!passwordRepeated.equals(password))
            nl.addErrorMessage(PASSWORDS_NOT_ALIKE);
        if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";            
        }
        /*nl = Checks.isPasswordOK(password);
        if (nl.errorsExist()) {
            nl.printOutMessages();
            return "";            
        } */               
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        List<User> users = s.createQuery("from User where username = :username").setParameter("username", username).getResultList();
        if (!users.isEmpty()) 
            nl.addErrorMessage(USERNAME_TAKEN);
        else {
            User u = new User(username, firstName, lastName, password, phone, email);
            s.save(u);
            nl.addInfoMessage(SUCCESS);
        }
        
        String returnString = "";
        if (!nl.errorsExist()) 
            returnString = "/index";        
        nl.printOutMessages();
        return returnString;        
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeated() {
        return passwordRepeated;
    }

    public void setPasswordRepeated(String passwordRepeated) {
        this.passwordRepeated = passwordRepeated;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
}
