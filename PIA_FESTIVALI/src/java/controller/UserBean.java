package controller;

import hibernate.HibernateUtility;
import java.time.LocalDateTime;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import model.Festival;
import org.hibernate.Query;
import org.hibernate.Session;

@ManagedBean
@RequestScoped
public class UserBean {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loggedIn;
    
    private List<Festival> top5Recent;
    
    public UserBean() {
        LocalDateTime nLDT = LocalDateTime.now();
        
        Session s = HibernateUtility.getSessionFactory().openSession();
        s.beginTransaction();
        
        Query q = s.createQuery("from Festival where festivalFrom >= :now order by festivalFrom")
                .setParameter("now", nLDT);
        q.setMaxResults(5);
        top5Recent = q.getResultList();
        
        s.getTransaction().commit();
        s.close();
        
    }

    public LoginBean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(LoginBean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public List<Festival> getTop5Recent() {
        return top5Recent;
    }

    public void setTop5Recent(List<Festival> top5Recent) {
        this.top5Recent = top5Recent;
    }
    
    
    
}
