package hibernate;

import java.io.Serializable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtility {
    
    private static SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {     
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
        .configure( "hibernate.cfg.xml" )
        .build();

        Metadata metadata = new MetadataSources( standardRegistry )
        .getMetadataBuilder()
        .build();
     
        return metadata.getSessionFactoryBuilder().build();    
     
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    public static Serializable saveObjectToDB(Object toBeSaved) {
        Serializable ret = null;
        Session s = buildSessionFactory().openSession();
        s.beginTransaction();
        ret = s.save(toBeSaved);
        s.getTransaction().commit();
        s.close();                    
        return ret;
    }
    
    public static void saveOrUpdateObjectInDB(Object toBeSaved) {
        Session s = buildSessionFactory().openSession();
        s.beginTransaction();
        s.saveOrUpdate(toBeSaved);
        s.getTransaction().commit();
        s.close();                    
    }
    
    public static void updateObjectInDB(Object toBeUpdated) {
        Session s = buildSessionFactory().openSession();
        s.beginTransaction();
        s.update(toBeUpdated);
        s.getTransaction().commit();
        s.close();      
    }
    
    
}
