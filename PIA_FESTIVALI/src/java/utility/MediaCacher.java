package utility;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Media;

public class MediaCacher {

    private static final String FILE_CACHE_LOCATION = "c:/Users/Dusan/Documents/NetBeansProjects/PIA_FESTIVALI/web/uploads/";

    public static String saveToLocation(byte[] file, String filename) {

        String filepath = FILE_CACHE_LOCATION + filename;

        try {
            Path path = Paths.get(filepath);
            Files.deleteIfExists(path);  
            Files.createFile(path);
            Files.write(path, file);            
        } catch (IOException ex) {
            Logger.getLogger(MediaCacher.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "/uploads/" + filename;

    }
    
    public static String saveToLocation(Media m) {
        return saveToLocation(m.getMedia(), m.getFilename());
    }

}
