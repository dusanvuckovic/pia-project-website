package utility;

import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class NotificationList {       
    
    private final List<FacesMessage> messages = new ArrayList<>();
    
    public NotificationList addErrorMessage(String text) {
        messages.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, text, ""));            
        return this;
    }
    
    public NotificationList addInfoMessage(String text) {
        messages.add(new FacesMessage(FacesMessage.SEVERITY_INFO, text, ""));            
        return this;
    }
    
    public boolean errorsExist() {
        for (FacesMessage m: messages)
            if (m.getSeverity() == FacesMessage.SEVERITY_ERROR)
                return true;
        return false;
    }
    
    public boolean printOutMessagesIfError() {
        if (errorsExist()) {
            printOutMessages();
            return true;
        }
        return false;        
    }
    
    public void printOutMessages() {
        for (FacesMessage m: messages)
            FacesContext.getCurrentInstance().addMessage(null, m);
        messages.clear();
    }
        
}
