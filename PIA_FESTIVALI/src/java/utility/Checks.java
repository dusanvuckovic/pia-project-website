package utility;

import java.util.Arrays;
import java.util.List;

public class Checks {

    public static NotificationList isPasswordOK(String password) {

        final List<Character> specials = Arrays.asList('/', '\\', '^', '$', '.', '|', '?', '*', '+', '(', ')', '[', '{');
        NotificationList nl = new NotificationList();

        if (password.length() < 6) {
            nl.addErrorMessage("Lozinka ne može biti kraća od 6 karaktera!");
        } else {
            nl.addErrorMessage("Lozinka ne može biti duža od 12 karaktera!");
        }
        char first = password.toCharArray()[0];
        if (Character.isUpperCase(first) || Character.isLowerCase(first)) {
            nl.addErrorMessage("Mora počinjati slovom!");
        }

        int uppercaseNumber = 0, lowercaseNumber = 0, digitNumber = 0, specialCharactersNumber = 0;
        for (char c : password.toCharArray()) {
            if (Character.isLetter(c)) {
                if (Character.isUpperCase(c)) {
                    ++uppercaseNumber;
                } else if (Character.isLowerCase(c)) {
                    ++lowercaseNumber;
                }
            } else if (Character.isDigit(c)) {
                ++digitNumber;
            } else if (specials.contains(c)) {
                ++specialCharactersNumber;
            }
        }
        if (uppercaseNumber < 1) {
            nl.addErrorMessage("Lozinka mora imati makar 1 veliko slovo!");
        }
        if (lowercaseNumber < 3) {
            nl.addErrorMessage("Lozinka mora imati makar 3 mala slova!");
        }
        if (digitNumber < 2) {
            nl.addErrorMessage("Lozinka mora imati barem 2 numerika!");
        }
        if (specialCharactersNumber < 2) {
            nl.addErrorMessage("Lozinka mora imati barem 2 specijalna karaktera!");
        }

        for (int i = 0; i < password.length() - 2; i++) {
            char c1 = password.charAt(i), c2 = password.charAt(i + 1), c3 = password.charAt(i + 2);
            if (c1 == c2 && c2 == c3) {
                nl.addErrorMessage("Lozinka ne sme imati više od 2 uzastopna ista karaktera!");
                break;
            }
        }

        return nl;
    }
    
    public static boolean isPasswordOKBool(String password) {
        NotificationList nl = isPasswordOK(password);
        return !nl.errorsExist();        
    }
    
     public static boolean isPhoneValid(String phone) {
        for (char c: phone.toCharArray())
            if (!Character.isDigit(c))
                return false;
        if (!phone.startsWith("06"))
            return false;        
        if (phone.length() > 10 || phone.length() < 9)
            return false;
        return true;
    }
    
    public static boolean isEmailValid(String email) {
        if (!email.contains("@") || !email.contains("."))
            return false;
        if (!Character.isAlphabetic(email.toCharArray()[0]))
            return false;
        return true;
    }    

}
