package json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Festival;
import model.additional.PerformanceH;

public class JSONParser implements Serializable {
    
    private final String JSON_DATE_PATTERN = "dd/MM/yyyy",
                         JSON_TIME_PATTERN = "hh:mm:ss a";
    
    private final Gson gson = new GsonBuilder().registerTypeAdapter(
                LocalDate.class, (JsonDeserializer<LocalDate>) (json, type, jsonDeserializationContext)
                -> LocalDate.parse(json.getAsJsonPrimitive().getAsString(), DateTimeFormatter.ofPattern(JSON_DATE_PATTERN))
        ).registerTypeAdapter(
                LocalTime.class, (JsonDeserializer<LocalTime>) (json, type, jsonDeserializationContext)
                -> LocalTime.parse(json.getAsJsonPrimitive().getAsString(), DateTimeFormatter.ofPattern(JSON_TIME_PATTERN))
        )
                .create();
    private JSONMain json = null;
    
    private Festival festival;
    private List<PerformanceH> performances;
    
    public JSONParser(InputStream input) {
        try {
            json = gson.fromJson(new InputStreamReader(input, "UTF-8"), json.JSONMain.class);
            makeFestival();
            makePerformances();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(JSONParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public Festival getFestival() {
        return festival;    
    }
    public List<PerformanceH> getPerformances() {
        return performances;
    }
    
    private void makeFestival() {
        JSONFestival f = json.Festival;
        
        String facebook = "",
               twitter = "",
               instagram = "",
               youtube = "";
        
        for (JSONSocialNetwork jsn: f.SocialNetworks) {
            switch (jsn.Name) {
                case "Facebook":  facebook = jsn.Link; break;
                case "Twitter":   twitter = jsn.Link; break;
                case "YouTube":   youtube = jsn.Link; break;
                case "Instagram": instagram = jsn.Link; break;
            }
        }
        
        festival = new Festival(0, f.Name, f.Place, f.StartDate.atStartOfDay(), f.EndDate.atStartOfDay(),
                f.Tickets[2], f.Tickets[0], f.Tickets[3], f.Tickets[1], facebook, twitter, instagram, youtube);
        festival.setFestivalYear(festival.getFestivalTo().getYear());
        
    }
    
    private void makePerformances() {
        performances = new ArrayList<>();
        for (JSONPerformers perf: json.Festival.PerformersList) {
            performances.add(new PerformanceH(perf.Performer, perf.makeDateFrom(), perf.makeDateTo()));
        }
    }
    
}
