package json;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.additional.PerformanceH;

public class JSONFestival implements Serializable {

    String Name;
    String Place;
    LocalDate StartDate;
    LocalDate EndDate;
    Integer[] Tickets;
    JSONPerformers[] PerformersList;
    JSONSocialNetwork[] SocialNetworks;     
    
}
