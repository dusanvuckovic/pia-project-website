package json;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import utility.Conversions;

public class JSONPerformers implements Serializable {

    String Performer;
    LocalDate StartDate;
    LocalDate EndDate;
    LocalTime StartTime;
    LocalTime EndTime;
    
    Date makeDateFrom() {
        return Conversions.toDate(LocalDateTime.of(StartDate, StartTime));
    }
    
    Date makeDateTo() {
        return Conversions.toDate(LocalDateTime.of(EndDate, EndTime));    
    }
    
}
