package csv;

import controller.AddFestivalFileBean;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Festival;
import model.additional.PerformanceH;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import utility.Conversions;

public class CSVParser {

    private final String CSV_DATE_PATTERN = "dd/MM/yyyy";
    private final String CSV_DATE_TIME_PATTERN = "dd/MM/yyyy hh:mm:ss a";

    public enum CSVEnum {
        INTERMEDIARY, FESTIVAL, TICKET, PERFORMANCE, SOCIAL
    };

    private static final String CSV_FEST = "$Festival$,$Place$,$StartDate$,$EndDate$".replace('$', '"'),
            CSV_TICKET = "$TicketType$, $Price$".replace('$', '"'),
            CSV_PERFORMANCE = "$Performer$,$StartDate$,$EndDate$,$StartTime$,$EndTime$".replace('$', '"'),
            CSV_SOCIAL = "$Social Network$,$Link$".replace('$', '"');

    private static final Map<String, CSVEnum> CSV_MAPPING;

    static {
        CSV_MAPPING = new HashMap();
        CSV_MAPPING.put("", CSVEnum.INTERMEDIARY);
        CSV_MAPPING.put(CSV_FEST, CSVEnum.FESTIVAL);
        CSV_MAPPING.put(CSV_TICKET, CSVEnum.TICKET);
        CSV_MAPPING.put(CSV_PERFORMANCE, CSVEnum.PERFORMANCE);
        CSV_MAPPING.put(CSV_SOCIAL, CSVEnum.SOCIAL);
    }

    private CSVEnum csvState = CSVEnum.INTERMEDIARY;

    private boolean switchCSVState(String newState) {
        if (CSV_MAPPING.containsKey(newState)) {
            csvState = CSV_MAPPING.get(newState);
            return true;
        }
        return false;
    }

    private Festival f;
    private List<PerformanceH> performances;

    public CSVParser(InputStream input) {
        processCSV(input);
    }
    
    public Festival getFestival() {
        return f;
    }
    public List<PerformanceH> getPerformances() {
        return performances;
    }

    private void processCSV(InputStream input) {
        csvState = CSVEnum.INTERMEDIARY;
        f = new Festival();
        performances = new ArrayList();

        Scanner lineScanner = new Scanner(input);
        while (lineScanner.hasNextLine()) {
            String line = lineScanner.nextLine();
            if (switchCSVState(line)) {
                continue;
            }
            switch (csvState) {
                case PERFORMANCE:
                    csvPerformance(line);
                    break;
                case FESTIVAL:
                    csvFestival(line);
                    break;
                case SOCIAL:
                    csvSocial(line);
                    break;
                case TICKET:
                    csvTicket(line);
                    break;
                default:
                    break;
            }
        }
        f.setFestivalYear(f.getFestivalTo().getYear());
    }

    private String parseStringFromRecord(CSVRecord csv, String title) {
        String data = csv.get(title);
        while (data.endsWith(" ") || data.endsWith("\"")) {
            data = data.substring(0, data.length() - 1);
        }
        while (data.startsWith(" ") || data.startsWith("\"")) {
            data = data.substring(1);
        }
        return data;
    }

    private void csvFestival(String line) {

        try {

            CSVRecord r = CSVFormat.RFC4180.withHeader("Festival", "Place", "StartDate", "EndDate").
                    parse(new StringReader(line)).getRecords().get(0);

            String name = parseStringFromRecord(r, "Festival");
            String location = parseStringFromRecord(r, "Place");
            String dateFrom = parseStringFromRecord(r, "StartDate");
            String dateTo = parseStringFromRecord(r, "EndDate");

            LocalDateTime from = LocalDate.parse(dateFrom, DateTimeFormatter.ofPattern(CSV_DATE_PATTERN)).atStartOfDay();
            LocalDateTime to = LocalDate.parse(dateTo, DateTimeFormatter.ofPattern(CSV_DATE_PATTERN)).atStartOfDay();

            f.setFestivalTitle(name);
            f.setPlace(location);
            f.setFestivalFrom(from);
            f.setFestivalTo(to);

        } catch (IOException ex) {
            Logger.getLogger(AddFestivalFileBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void csvPerformance(String line) {

        try {

            CSVRecord r = CSVFormat.RFC4180.withHeader("Performer", "StartDate", "EndDate", "StartTime", "EndTime").
                    parse(new StringReader(line)).getRecords().get(0);

            String performer = parseStringFromRecord(r, "Performer");
            String fromDate = parseStringFromRecord(r, "StartDate");
            String toDate = parseStringFromRecord(r, "EndDate");
            String fromTime = parseStringFromRecord(r, "StartTime");
            String toTime = parseStringFromRecord(r, "EndTime");

            LocalDateTime from = LocalDateTime.parse(fromDate + " " + fromTime, DateTimeFormatter.ofPattern(CSV_DATE_TIME_PATTERN)),
                    to = LocalDateTime.parse(toDate + " " + toTime, DateTimeFormatter.ofPattern(CSV_DATE_TIME_PATTERN));
            performances.add(new PerformanceH(performer, Conversions.toDate(from), Conversions.toDate(to)));

        } catch (IOException ex) {
            Logger.getLogger(AddFestivalFileBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void csvSocial(String line) {
        try {
            CSVRecord r = CSVFormat.RFC4180.withHeader("Social Network", "Link").
                    parse(new StringReader(line)).getRecords().get(0);

            String type = parseStringFromRecord(r, "Social Network");
            String value = parseStringFromRecord(r, "Link");

            switch (type) {
                case "Facebook":
                    f.setFacebook(value);
                    break;
                case "Twitter":
                    f.setTwitter(value);
                    break;
                case "Instagram":
                    f.setInstagram(value);
                    break;
                case "YouTube":
                    f.setYoutube(value);
                    break;

            }
        } catch (IOException ex) {
            Logger.getLogger(AddFestivalFileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void csvTicket(String line) {
        try {
            CSVRecord r = CSVFormat.RFC4180.withHeader("TicketType", "Price").
                    parse(new StringReader(line)).getRecords().get(0);

            String type = parseStringFromRecord(r, "TicketType");
            String value = parseStringFromRecord(r, "Price");
            int intValue = Integer.parseInt(value);

            switch (type) {
                case "per day":
                    f.setTicketPriceDay(intValue);
                    break;
                case "whole festival":
                    f.setTicketPricePackage(intValue);
                    break;
                case "available day":
                    f.setTicketNumberDay(intValue);
                    break;
                case "available package":
                    f.setTicketNumberPackage(intValue);
                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(AddFestivalFileBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
