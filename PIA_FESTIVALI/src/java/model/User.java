
package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userID;
    @Column(nullable=false, length=50)
    private String username;
    @Column(nullable=false, length=50)
    private String firstName;
    @Column(nullable=false, length=50)
    private String lastName;
    @Column(nullable=false, length=50)
    private String password;
    @Column(nullable=false, length=50)
    private String phone;
    @Column(nullable=false, length=50)
    private String email;
    @Column(nullable=false)
    private boolean administrator;
    @Column(nullable=false)
    private boolean approved;
    @Column(nullable=false)
    private boolean denied;
    @Column(nullable=false)
    private int failedReservations;
    @Column(nullable=false)
    private LocalDateTime lastLogin;

    public User() {
    }

    public User(String username, String firstName, String lastName, String password, String phone, String email) {
        this.userID = 0;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.phone = phone;
        this.email = email;
        this.administrator = false;
        this.approved = false;
        this.denied = false;
        this.failedReservations = 0;
        this.lastLogin = LocalDateTime.now();
    }
    
    public String returnFullName() {
        return firstName + " " + lastName;
    }
    
    public boolean bannedFromMakingReservations() {
        return failedReservations >= 3;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isDenied() {
        return denied;
    }

    public void setDenied(boolean denied) {
        this.denied = denied;
    }

    public int getFailedReservations() {
        return failedReservations;
    }

    public void setFailedReservations(int failedReservations) {
        this.failedReservations = failedReservations;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.userID;
        hash = 73 * hash + Objects.hashCode(this.username);
        hash = 73 * hash + Objects.hashCode(this.firstName);
        hash = 73 * hash + Objects.hashCode(this.lastName);
        hash = 73 * hash + Objects.hashCode(this.password);
        hash = 73 * hash + Objects.hashCode(this.phone);
        hash = 73 * hash + Objects.hashCode(this.email);
        hash = 73 * hash + (this.administrator ? 1 : 0);
        hash = 73 * hash + (this.approved ? 1 : 0);
        hash = 73 * hash + (this.denied ? 1 : 0);
        hash = 73 * hash + this.failedReservations;
        hash = 73 * hash + Objects.hashCode(this.lastLogin);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.userID != other.userID) {
            return false;
        }
        if (this.administrator != other.administrator) {
            return false;
        }
        if (this.approved != other.approved) {
            return false;
        }
        if (this.denied != other.denied) {
            return false;
        }
        if (this.failedReservations != other.failedReservations) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.lastLogin, other.lastLogin)) {
            return false;
        }
        return true;
    }

   
    
    

}