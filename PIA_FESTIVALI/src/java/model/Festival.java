package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "festivals")
public class Festival implements Serializable {
        
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int festivalID;
    @Column(nullable=false, length=50)
    private String festivalTitle;
    @Column(nullable=false)
    private int festivalYear;    
    @Column(nullable=false, length=50)
    private String place;
    @Column(nullable=false)
    private LocalDateTime festivalFrom;
    @Column(nullable=false)
    private LocalDateTime festivalTo;	
    @Column(nullable=false)
    private int ticketNumberDay;
    @Column(nullable=false)    
    private int ticketPriceDay;
    @Column(nullable=false)
    private int ticketNumberPackage;    
    @Column(nullable=false)
    private int ticketPricePackage;
    @Column(nullable=false)
    private int ticketSoldPackage;
    @Column(nullable=false)
    private int maxReservations;
    @Column(nullable=false, length=50)
    private String facebook;
    @Column(nullable=false, length=50)
    private String twitter;
    @Column(nullable=false, length=50)
    private String instagram;
    @Column(nullable=false, length=50)
    private String youtube;     
    @Column(nullable=false)
    private int viewCount;
    @Column(nullable=false)    
    private boolean renderSearch;
    @Column(nullable=false)    
    private boolean renderSelect;
    @Column(nullable=false)
    private boolean renderReservation;
    @Column(nullable=false)
    private int currentStep;

    public Festival() {        
        currentStep = 1;
    }
        
    public boolean renderFacebook() {
        return !"".equals(facebook);
    }
    
    public boolean renderTwitter() {
        return !"".equals(twitter);    
    }
    
    public boolean renderInstagram() {
        return !"".equals(instagram);    
    }
    
    public boolean renderYouTube() {
        return !"".equals(youtube);    
    }
    
    public int getNumberOfDays() {
        return (int) ChronoUnit.DAYS.between(festivalFrom, festivalTo) + 1;
    }
    
    public int getSpecificDay(LocalDateTime performanceStart) {
        return (int) ChronoUnit.DAYS.between(festivalFrom, performanceStart) + 1;
    }

    public Festival(int festivalID, String festivalTitle, String place, LocalDateTime festivalFrom, LocalDateTime festivalTo, 
            int ticketNumberDay, int ticketPriceDay, int ticketNumberPackage, int ticketPricePackage, 
            String facebook, String twitter, String instagram, String youtube) {
        this.festivalID = festivalID;
        this.festivalTitle = festivalTitle;
        this.festivalYear = festivalTo.getYear();
        this.place = place;
        this.festivalFrom = festivalFrom;
        this.festivalTo = festivalTo;
        this.ticketNumberDay = ticketNumberDay;
        this.ticketPriceDay = ticketPriceDay;
        this.ticketNumberPackage = ticketNumberPackage;
        this.ticketPricePackage = ticketPricePackage;
        this.ticketSoldPackage = 0;
        this.maxReservations = 5;
        this.facebook = facebook;
        this.twitter = twitter;        
        this.instagram = instagram;
        this.youtube = youtube;
        this.renderReservation = this.renderSearch = this.renderSelect = false;
        this.viewCount = 0;
        this.currentStep = 1;
    }
    
    public int getActiveIndex() {                    
        if (currentStep == 1)
            return 0;
        if (currentStep == 2)
            return 2;        
        return 4;
    }
    
    public void incrementViewCount() {
        ++viewCount;
    }
    
    public String getFestivalLabel() {
        return festivalTitle + " " + Integer.toString(festivalYear);
    }
    
    
    public boolean isInDB() {
        return currentStep != 1;
    }
    
    public boolean renderStep2() {
        return currentStep != 1;
    }
    public boolean renderStep3() {
        return (currentStep ==0 || currentStep > 2);
    }

    public int getFestivalID() {
        return festivalID;
    }

    public void setFestivalID(int festivalID) {
        this.festivalID = festivalID;
    }

    public String getFestivalTitle() {
        return festivalTitle;
    }

    public void setFestivalTitle(String festivalTitle) {
        this.festivalTitle = festivalTitle;
    }

    public int getFestivalYear() {
        return festivalYear;
    }

    public void setFestivalYear(int festivalYear) {
        this.festivalYear = festivalYear;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public LocalDateTime getFestivalFrom() {
        return festivalFrom;
    }

    public void setFestivalFrom(LocalDateTime festivalFrom) {
        this.festivalFrom = festivalFrom;
    }

    public LocalDateTime getFestivalTo() {
        return festivalTo;
    }

    public void setFestivalTo(LocalDateTime festivalTo) {
        this.festivalTo = festivalTo;
    }

    public int getTicketNumberDay() {
        return ticketNumberDay;
    }

    public void setTicketNumberDay(int ticketNumberDay) {
        this.ticketNumberDay = ticketNumberDay;
    }

    public int getTicketPriceDay() {
        return ticketPriceDay;
    }

    public void setTicketPriceDay(int ticketPriceDay) {
        this.ticketPriceDay = ticketPriceDay;
    }

    public int getTicketNumberPackage() {
        return ticketNumberPackage;
    }

    public void setTicketNumberPackage(int ticketNumberPackage) {
        this.ticketNumberPackage = ticketNumberPackage;
    }

    public int getTicketPricePackage() {
        return ticketPricePackage;
    }

    public void setTicketPricePackage(int ticketPricePackage) {
        this.ticketPricePackage = ticketPricePackage;
    }

    public int getTicketSoldPackage() {
        return ticketSoldPackage;
    }

    public void setTicketSoldPackage(int ticketSoldPackage) {
        this.ticketSoldPackage = ticketSoldPackage;
    }

    public int getMaxReservations() {
        return maxReservations;
    }

    public void setMaxReservations(int maxReservations) {
        this.maxReservations = maxReservations;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public boolean isRenderSearch() {
        return renderSearch;
    }

    public void setRenderSearch(boolean renderSearch) {
        this.renderSearch = renderSearch;
    }

    public boolean isRenderSelect() {
        return renderSelect;
    }

    public void setRenderSelect(boolean renderSelect) {
        this.renderSelect = renderSelect;
    }

    public boolean isRenderReservation() {
        return renderReservation;
    }

    public void setRenderReservation(boolean renderReservation) {
        this.renderReservation = renderReservation;
    }

    public int getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.festivalID;
        hash = 67 * hash + Objects.hashCode(this.festivalTitle);
        hash = 67 * hash + this.festivalYear;
        hash = 67 * hash + Objects.hashCode(this.place);
        hash = 67 * hash + Objects.hashCode(this.festivalFrom);
        hash = 67 * hash + Objects.hashCode(this.festivalTo);
        hash = 67 * hash + this.ticketNumberDay;
        hash = 67 * hash + this.ticketPriceDay;
        hash = 67 * hash + this.ticketNumberPackage;
        hash = 67 * hash + this.ticketPricePackage;
        hash = 67 * hash + this.ticketSoldPackage;
        hash = 67 * hash + this.maxReservations;
        hash = 67 * hash + Objects.hashCode(this.facebook);
        hash = 67 * hash + Objects.hashCode(this.twitter);
        hash = 67 * hash + Objects.hashCode(this.instagram);
        hash = 67 * hash + Objects.hashCode(this.youtube);
        hash = 67 * hash + this.viewCount;
        hash = 67 * hash + (this.renderSearch ? 1 : 0);
        hash = 67 * hash + (this.renderSelect ? 1 : 0);
        hash = 67 * hash + (this.renderReservation ? 1 : 0);
        hash = 67 * hash + this.currentStep;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Festival other = (Festival) obj;
        if (this.festivalID != other.festivalID) {
            return false;
        }
        if (this.festivalYear != other.festivalYear) {
            return false;
        }
        if (this.ticketNumberDay != other.ticketNumberDay) {
            return false;
        }
        if (this.ticketPriceDay != other.ticketPriceDay) {
            return false;
        }
        if (this.ticketNumberPackage != other.ticketNumberPackage) {
            return false;
        }
        if (this.ticketPricePackage != other.ticketPricePackage) {
            return false;
        }
        if (this.ticketSoldPackage != other.ticketSoldPackage) {
            return false;
        }
        if (this.maxReservations != other.maxReservations) {
            return false;
        }
        if (this.viewCount != other.viewCount) {
            return false;
        }
        if (this.renderSearch != other.renderSearch) {
            return false;
        }
        if (this.renderSelect != other.renderSelect) {
            return false;
        }
        if (this.renderReservation != other.renderReservation) {
            return false;
        }
        if (this.currentStep != other.currentStep) {
            return false;
        }
        if (!Objects.equals(this.festivalTitle, other.festivalTitle)) {
            return false;
        }
        if (!Objects.equals(this.place, other.place)) {
            return false;
        }
        if (!Objects.equals(this.facebook, other.facebook)) {
            return false;
        }
        if (!Objects.equals(this.twitter, other.twitter)) {
            return false;
        }
        if (!Objects.equals(this.instagram, other.instagram)) {
            return false;
        }
        if (!Objects.equals(this.youtube, other.youtube)) {
            return false;
        }
        if (!Objects.equals(this.festivalFrom, other.festivalFrom)) {
            return false;
        }
        if (!Objects.equals(this.festivalTo, other.festivalTo)) {
            return false;
        }
        return true;
    }

    
   

}
