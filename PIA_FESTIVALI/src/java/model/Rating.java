package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ratings")
public class Rating implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ratingID;
    @Column(nullable=false)
    private int userID;
    @Column(nullable=false, length=50)    
    private String username;    
    @Column(nullable=false)
    private int festivalID;
    @Column(nullable=false, length=50)    
    private String festivalName;
    @Column(nullable=false)
    private int rating;
    @Column(nullable=false, length=200)
    private String comment;            
    @Column(nullable=false)
    private LocalDateTime madeOn;

    public Rating() {
    }

    public Rating(int userID, String username, int festivalID, String festivalName, int rating, String comment) {
        this.ratingID = 0;
        this.userID = userID;
        this.username = username;
        this.festivalID = festivalID;
        this.festivalName = festivalName;
        this.rating = rating;
        this.comment = comment;
        this.madeOn = LocalDateTime.now();
    }

    public int getRatingID() {
        return ratingID;
    }

    public void setRatingID(int ratingID) {
        this.ratingID = ratingID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getFestivalID() {
        return festivalID;
    }

    public void setFestivalID(int festivalID) {
        this.festivalID = festivalID;
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getMadeOn() {
        return madeOn;
    }

    public void setMadeOn(LocalDateTime madeOn) {
        this.madeOn = madeOn;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.ratingID;
        hash = 71 * hash + this.userID;
        hash = 71 * hash + Objects.hashCode(this.username);
        hash = 71 * hash + this.festivalID;
        hash = 71 * hash + Objects.hashCode(this.festivalName);
        hash = 71 * hash + this.rating;
        hash = 71 * hash + Objects.hashCode(this.comment);
        hash = 71 * hash + Objects.hashCode(this.madeOn);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rating other = (Rating) obj;
        if (this.ratingID != other.ratingID) {
            return false;
        }
        if (this.userID != other.userID) {
            return false;
        }
        if (this.festivalID != other.festivalID) {
            return false;
        }
        if (this.rating != other.rating) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.festivalName, other.festivalName)) {
            return false;
        }
        if (!Objects.equals(this.comment, other.comment)) {
            return false;
        }
        if (!Objects.equals(this.madeOn, other.madeOn)) {
            return false;
        }
        return true;
    }
    
   
}
