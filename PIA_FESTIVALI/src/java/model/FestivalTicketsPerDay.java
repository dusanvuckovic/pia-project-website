package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "festivaltickets")
public class FestivalTicketsPerDay implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;
    @Column(nullable=false)
    private int festivalID;
    @Column(nullable=false)
    private int festivalDay;
    @Column(nullable=false)
    private int ticketsRemaining;

    public FestivalTicketsPerDay() {
    }

    public FestivalTicketsPerDay(int festivalID, int festivalDay, int ticketsRemaining) {
        this.ID = 0;
        this.festivalID = festivalID;
        this.festivalDay = festivalDay;
        this.ticketsRemaining = ticketsRemaining;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getFestivalID() {
        return festivalID;
    }

    public void setFestivalID(int festivalID) {
        this.festivalID = festivalID;
    }

    public int getFestivalDay() {
        return festivalDay;
    }

    public void setFestivalDay(int festivalDay) {
        this.festivalDay = festivalDay;
    }

    public int getTicketsRemaining() {
        return ticketsRemaining;
    }

    public void setTicketsRemaining(int ticketsRemaining) {
        this.ticketsRemaining = ticketsRemaining;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.ID;
        hash = 83 * hash + this.festivalID;
        hash = 83 * hash + this.festivalDay;
        hash = 83 * hash + this.ticketsRemaining;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FestivalTicketsPerDay other = (FestivalTicketsPerDay) obj;
        if (this.ID != other.ID) {
            return false;
        }
        if (this.festivalID != other.festivalID) {
            return false;
        }
        if (this.festivalDay != other.festivalDay) {
            return false;
        }
        if (this.ticketsRemaining != other.ticketsRemaining) {
            return false;
        }
        return true;
    }
    
}
