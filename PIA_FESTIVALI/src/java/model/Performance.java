package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import model.additional.PerformanceH;
import utility.Conversions;

@Entity
@Table(name="performances")
public class Performance implements Serializable, Comparable {    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int performanceID;
    @Column(nullable=false)
    private int festivalID;
    @Column(nullable=false)
    private int festivalDay;
    @Column(nullable=false, length=50)
    private String performer;
    @Column(nullable=false)
    private LocalDateTime performanceStart;
    @Column(nullable=false)
    private LocalDateTime performanceEnd;

    public Performance() {
    }
    
    public Performance(Festival f, PerformanceH p) {
        this.performanceID = 0;
        this.festivalID = f.getFestivalID();
        this.performer = p.getPerformer();
        this.performanceStart = Conversions.toLDT(p.getFrom());
        this.performanceEnd = Conversions.toLDT(p.getTo());
        this.festivalDay = f.getSpecificDay(performanceStart);
    
    }

    public int getPerformanceID() {
        return performanceID;
    }

    public void setPerformanceID(int performanceID) {
        this.performanceID = performanceID;
    }

    public int getFestivalID() {
        return festivalID;
    }

    public void setFestivalID(int festivalID) {
        this.festivalID = festivalID;
    }
    
    public int getFestivalDay() {
        return festivalDay;
    }

    public void setFestivalDay(int day) {
        this.festivalDay = day;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public LocalDateTime getPerformanceStart() {
        return performanceStart;
    }

    public void setPerformanceStart(LocalDateTime start) {
        this.performanceStart = start;
    }

    public LocalDateTime getPerformanceEnd() {
        return performanceEnd;
    }

    public void setPerformanceEnd(LocalDateTime end) {
        this.performanceEnd = end;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.performanceID;
        hash = 53 * hash + this.festivalID;
        hash = 53 * hash + this.festivalDay;
        hash = 53 * hash + Objects.hashCode(this.performer);
        hash = 53 * hash + Objects.hashCode(this.performanceStart);
        hash = 53 * hash + Objects.hashCode(this.performanceEnd);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Performance other = (Performance) obj;
        if (this.performanceID != other.performanceID) {
            return false;
        }
        if (this.festivalID != other.festivalID) {
            return false;
        }
        if (this.festivalDay != other.festivalDay) {
            return false;
        }
        if (!Objects.equals(this.performer, other.performer)) {
            return false;
        }
        if (!Objects.equals(this.performanceStart, other.performanceStart)) {
            return false;
        }
        if (!Objects.equals(this.performanceEnd, other.performanceEnd)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object o) {
        Performance p1 = this, p2 = (Performance) o;
        LocalDateTime start1 = p1.performanceStart, start2 = p2.performanceStart;
        return start1.compareTo(start2);
    }
}