package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reservations")
public class Reservation implements Serializable {
        
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int reservationID;
    @Column(nullable=false)
    private int userID;
    @Column(nullable=false)
    private int festivalID;
    @Column(nullable=false)
    private int reservationDay;
    @Column(nullable=false)    
    private int numberOfTickets;
    @Column(nullable=false)
    private LocalDate madeOn;
    @Column(nullable=false)
    private boolean approved;
    @Column(nullable=false)
    private boolean denied;

    public Reservation() {
    }

    public Reservation(int userID, int festivalID, int reservationDay, int numberOfTickets) {
        this.reservationID = 0;
        this.userID = userID;
        this.festivalID = festivalID;
        this.reservationDay = reservationDay;
        this.numberOfTickets = numberOfTickets;
        this.madeOn = LocalDate.now();
        this.approved = this.denied = false;
    }    
    
    public String printOutDay() {
        if (reservationDay == 0)
            return "Komplet";
        return Integer.toString(reservationDay);
    }

    public int getReservationID() {
        return reservationID;
    }

    public void setReservationID(int reservationID) {
        this.reservationID = reservationID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getFestivalID() {
        return festivalID;
    }

    public void setFestivalID(int festivalID) {
        this.festivalID = festivalID;
    }

    public int getReservationDay() {
        return reservationDay;
    }

    public void setReservationDay(int reservationDay) {
        this.reservationDay = reservationDay;
    }

    public int getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(int numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public LocalDate getMadeOn() {
        return madeOn;
    }

    public void setMadeOn(LocalDate madeOn) {
        this.madeOn = madeOn;
    }

    public boolean isDenied() {
        return denied;
    }

    public void setDenied(boolean denied) {
        this.denied = denied;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.reservationID;
        hash = 79 * hash + this.userID;
        hash = 79 * hash + this.festivalID;
        hash = 79 * hash + this.reservationDay;
        hash = 79 * hash + this.numberOfTickets;
        hash = 79 * hash + Objects.hashCode(this.madeOn);
        hash = 79 * hash + (this.approved ? 1 : 0);
        hash = 79 * hash + (this.denied ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reservation other = (Reservation) obj;
        if (this.reservationID != other.reservationID) {
            return false;
        }
        if (this.userID != other.userID) {
            return false;
        }
        if (this.festivalID != other.festivalID) {
            return false;
        }
        if (this.reservationDay != other.reservationDay) {
            return false;
        }
        if (this.numberOfTickets != other.numberOfTickets) {
            return false;
        }
        if (this.approved != other.approved) {
            return false;
        }
        if (this.denied != other.denied) {
            return false;
        }
        if (!Objects.equals(this.madeOn, other.madeOn)) {
            return false;
        }
        return true;
    }
    
    
}
