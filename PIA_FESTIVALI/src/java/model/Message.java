/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="messages")
public class Message implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int messageID;
    @Column(nullable=false)
    private String message;
    @Column(nullable=false)
    private int userID;
    @Column(nullable=false)
    boolean shown;

    public Message() {
    }

    public Message(String message, int userID) {
        this.messageID = 0;
        this.message = message;
        this.userID = userID;
        this.shown = false;
    }

    public int getMessageID() {
        return messageID;
    }

    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
    
    public boolean isShown() {
        return shown;
    }

    public void setShown(boolean shown) {
        this.shown = shown;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.messageID;
        hash = 47 * hash + Objects.hashCode(this.message);
        hash = 47 * hash + this.userID;
        hash = 47 * hash + (this.shown ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Message other = (Message) obj;
        if (this.messageID != other.messageID) {
            return false;
        }
        if (this.userID != other.userID) {
            return false;
        }
        if (this.shown != other.shown) {
            return false;
        }
        if (!Objects.equals(this.message, other.message)) {
            return false;
        }
        return true;
    }
    
}
