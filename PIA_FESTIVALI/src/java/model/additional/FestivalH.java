package model.additional;

import java.io.Serializable;
import java.util.Objects;
import model.Festival;

public class FestivalH implements Serializable, Comparable {
    
    private Festival f;
    private Double ranking;

    public FestivalH() {
    }

    public FestivalH(Festival f, Double ranking) {
        this.f = f;
        this.ranking = ranking;
    }

    public Festival getF() {
        return f;
    }

    public void setF(Festival f) {
        this.f = f;
    }

    public Double getRanking() {
        return ranking;
    }

    public void setRanking(Double ranking) {
        this.ranking = ranking;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.f);
        hash = 67 * hash + Objects.hashCode(this.ranking);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FestivalH other = (FestivalH) obj;
        if (!Objects.equals(this.f, other.f)) {
            return false;
        }
        if (!Objects.equals(this.ranking, other.ranking)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object o) {
        FestivalH f1 = this, f2 = (FestivalH) o;
        Double d1 = f1.getRanking(), d2 = f2.getRanking();
        return d2.compareTo(d1);
    }
    
    
    
    
    
    
    
}
