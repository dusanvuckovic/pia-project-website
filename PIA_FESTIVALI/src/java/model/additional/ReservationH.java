package model.additional;

import java.io.Serializable;
import java.util.Objects;
import model.Reservation;

public class ReservationH implements Serializable {
    
    private final String AS_IS = "Ignoriši",
                         APPROVE = "Prihvati",
                         DENY   = "Odbij";
    
    private Reservation r;
    private String username;
    private String festivalName;
    private String festivalYear;
    private String outcome;

    public ReservationH() {
    }

    public ReservationH(Reservation r, String username, String festivalName, String festivalYear) {
        this.r = r;
        this.username = username;
        this.festivalName = festivalName;
        this.festivalYear = festivalYear;
        this.outcome = AS_IS;
    }
    
    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public String getFestivalYear() {
        return festivalYear;
    }

    public void setFestivalYear(String festivalYear) {
        this.festivalYear = festivalYear;
    }

    public Reservation getR() {
        return r;
    }

    public void setR(Reservation r) {
        this.r = r;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.AS_IS);
        hash = 79 * hash + Objects.hashCode(this.APPROVE);
        hash = 79 * hash + Objects.hashCode(this.DENY);
        hash = 79 * hash + Objects.hashCode(this.r);
        hash = 79 * hash + Objects.hashCode(this.username);
        hash = 79 * hash + Objects.hashCode(this.festivalName);
        hash = 79 * hash + Objects.hashCode(this.festivalYear);
        hash = 79 * hash + Objects.hashCode(this.outcome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReservationH other = (ReservationH) obj;
        if (!Objects.equals(this.AS_IS, other.AS_IS)) {
            return false;
        }
        if (!Objects.equals(this.APPROVE, other.APPROVE)) {
            return false;
        }
        if (!Objects.equals(this.DENY, other.DENY)) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.festivalName, other.festivalName)) {
            return false;
        }
        if (!Objects.equals(this.festivalYear, other.festivalYear)) {
            return false;
        }
        if (!Objects.equals(this.outcome, other.outcome)) {
            return false;
        }
        if (!Objects.equals(this.r, other.r)) {
            return false;
        }
        return true;
    }
}
