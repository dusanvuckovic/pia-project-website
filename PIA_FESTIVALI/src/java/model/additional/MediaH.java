package model.additional;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import model.Media;

public class MediaH implements Serializable {
    
    public enum MediaEnum {
        NEW_ADD_TO, NEW_CANCELLED, DB_AS_IS, DB_DELETE
    }
    
    private Media m;
    private String mediaLocation;
    private MediaEnum state;
    
    public boolean isSameImage(MediaH m) {
        return this.m.isSameImage(m.m);
    }
    
    public MediaH() {
         state = MediaEnum.NEW_ADD_TO;
    }
    
    public MediaH(Media m, String mediaLocation) {
        this.m = m;
        this.mediaLocation = mediaLocation;
        this.state = MediaEnum.NEW_ADD_TO;
    }
    
    public MediaH(Media m, String mediaLocation, MediaEnum state) {
        this.m = m;
        this.mediaLocation = mediaLocation;
        this.state = state;
    }        
    
    public boolean shouldRender() {
        return (state == MediaEnum.NEW_ADD_TO || state == MediaEnum.DB_AS_IS); 
    }

    public Media getM() {
        return m;
    }

    public void setM(Media m) {
        this.m = m;
    }

    public String getMediaLocation() {
        return mediaLocation;
    }

    public void setMediaLocation(String mediaLocation) {
        this.mediaLocation = mediaLocation;
    }

    public MediaEnum getState() {
        return state;
    }

    public void setState(MediaEnum state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.m);
        hash = 59 * hash + Objects.hashCode(this.mediaLocation);
        hash = 59 * hash + Objects.hashCode(this.state);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MediaH other = (MediaH) obj;
        if (!Objects.equals(this.mediaLocation, other.mediaLocation)) {
            return false;
        }
        if (!Objects.equals(this.m, other.m)) {
            return false;
        }
        if (this.state != other.state) {
            return false;
        }
        return true;
    }
    
    
    
}
