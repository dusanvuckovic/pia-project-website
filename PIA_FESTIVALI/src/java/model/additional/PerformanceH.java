package model.additional;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import model.Performance;
import utility.Conversions;

 public class PerformanceH implements Serializable, Comparable {
                
        static int globalID = 0;
        
        private int ID;
        private String performer;
        private Date from;
        private Date to;        

        public PerformanceH() {     
            this.ID = ++globalID;
        }

        public PerformanceH(String performer, Date from, Date to) {
            this.ID = ++globalID;
            this.performer = performer;
            this.from = from;
            this.to = to;
        }
        
        public PerformanceH(Performance p) {
            this.ID = ++globalID;
            this.performer = p.getPerformer();
            this.from = Conversions.toDate(p.getPerformanceStart());
            this.to = Conversions.toDate(p.getPerformanceEnd());
        }

    public static int getGlobalID() {
        return globalID;
    }

    public static void setGlobalID(int globalID) {
        PerformanceH.globalID = globalID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.performer);
        hash = 23 * hash + Objects.hashCode(this.from);
        hash = 23 * hash + Objects.hashCode(this.to);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PerformanceH other = (PerformanceH) obj;
        if (!Objects.equals(this.performer, other.performer)) {
            return false;
        }
        return true;
    }          

    @Override
    public int compareTo(Object o) {
        PerformanceH p1 = this, p2 = (PerformanceH) o;
        return p1.from.compareTo(p2.from);
    }
       
    }