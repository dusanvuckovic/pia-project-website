package model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="media")
public class Media implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int mediaID;
    @Column(nullable=false)
    private int festivalID;
    @Column(nullable=false)
    private int userID;
    @Column(nullable=false, length=200)
    private String filename;
    @Column(nullable=false, length=200)
    private String contentType;
    @Column(nullable=false)    
    private byte[] media;
    @Column(nullable=false)
    private boolean approved;

    public Media() {
    }

    public Media(int festivalID, int userID, String filename, String contentType, byte[] media, boolean approved) {
        this.mediaID = 0;
        this.festivalID = festivalID;
        this.userID = userID;
        this.filename = filename;
        this.contentType = contentType;
        this.media = media;
        this.approved = approved;
    }
    
    public boolean isSameImage(Media m) {
        return Arrays.equals(this.media, m.media);
    }

    public int getMediaID() {
        return mediaID;
    }

    public void setMediaID(int mediaID) {
        this.mediaID = mediaID;
    }

    public int getFestivalID() {
        return festivalID;
    }

    public void setFestivalID(int festivalID) {
        this.festivalID = festivalID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getMedia() {
        return media;
    }

    public void setMedia(byte[] media) {
        this.media = media;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.mediaID;
        hash = 23 * hash + this.festivalID;
        hash = 23 * hash + this.userID;
        hash = 23 * hash + Objects.hashCode(this.filename);
        hash = 23 * hash + Objects.hashCode(this.contentType);
        hash = 23 * hash + Arrays.hashCode(this.media);
        hash = 23 * hash + (this.approved ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Media other = (Media) obj;
        if (this.mediaID != other.mediaID) {
            return false;
        }
        if (this.festivalID != other.festivalID) {
            return false;
        }
        if (this.userID != other.userID) {
            return false;
        }
        if (this.approved != other.approved) {
            return false;
        }
        if (!Objects.equals(this.filename, other.filename)) {
            return false;
        }
        if (!Objects.equals(this.contentType, other.contentType)) {
            return false;
        }
        if (!Arrays.equals(this.media, other.media)) {
            return false;
        }
        return true;
    }    
       
    
   }
